if (!file.Exists("autorun/vj_base_autorun.lua","LUA")) then return end
---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.Base 						= "weapon_vj_base"
SWEP.PrintName					= "AR2"
SWEP.Author 					= "Norpa"
SWEP.Contact					= "http://steamcommunity.com/groups/vrejgaming"
SWEP.Purpose					= "This weapon is made for Players and NPCs"
SWEP.Instructions				= "Controls are like a regular weapon."
SWEP.Category					= "VJ HL2+"
	-- Client Settings ---------------------------------------------------------------------------------------------------------------------------------------------
if (CLIENT) then
SWEP.Slot						= 2 -- Which weapon slot you want your SWEP to be in? (1 2 3 4 5 6) 
SWEP.SlotPos					= 4 -- Which part of that slot do you want the SWEP to be in? (1 2 3 4 5 6)
SWEP.UseHands					= true
end
	-- Main Settings ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.ViewModel					= "models/weapons/c_irifle.mdl"
SWEP.WorldModel					= "models/weapons/w_irifle.mdl"
SWEP.HoldType 					= "ar2"
SWEP.Spawnable					= false
SWEP.AdminSpawnable				= false

		-- NPC Settings ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.NPC_EnableDontUseRegulate 	= false -- Used for VJ Base SNPCs, if enabled the SNPC will remove use regulate
SWEP.NPC_NextPrimaryFire 		= 0.05 -- Next time it can use primary fire
SWEP.NPC_UsePistolBehavior 		= true -- Should it check the pistol activities when the NPC is firing the weapon?
	-- Primary Fire ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.Primary.Damage				= 8 -- Damage
SWEP.Primary.PlayerDamage		= 5 -- Put 1 to make it the same as above
SWEP.Primary.UseNegativePlayerDamage = false -- Should it use a negative number for the player damage?
SWEP.Primary.Force				= 10 -- Force applied on the object the bullet hits
SWEP.Primary.NumberOfShots		= 1 -- How many shots per attack?
SWEP.Primary.ClipSize			= 30 -- Max amount of bullets per clip
SWEP.Primary.DefaultClip		= 300 -- How much ammo do you get when you first pick up the weapon?
SWEP.Primary.Cone				= 2 -- How accurate is the bullet? (Players)
SWEP.Primary.Delay				= 0.1 -- Time until it can shoot again
SWEP.Primary.Tracer				= 1
SWEP.Primary.TracerType			= "AR2Tracer" -- Tracer type (Examples: AR2, laster, 9mm)
SWEP.Primary.TakeAmmo			= 1 -- How much ammo should it take on each shot?
SWEP.Primary.Automatic			= true -- Is it automatic?
SWEP.Primary.Ammo				= "AR2" -- Ammo type
SWEP.Primary.Sound				= "weapons/ar2/fire.wav"
SWEP.Primary.HasDistantSound	= true -- Does it have a distant sound when the gun is shot?
SWEP.Primary.DistantSound		=  "weapons/ar1/ar1_dist1.wav"
SWEP.Primary.DistantSoundVolume	= 0.3 -- Distant sound volume
SWEP.PrimaryEffects_MuzzleFlash = false

	-- Reload Settings ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.HasReloadSound				= false -- Does it have a reload sound? Remember even if this is set to false, the animation sound will still play!
SWEP.ReloadSound				= "weapons/ar2/ar2_reload.wav"
SWEP.Reload_TimeUntilAmmoIsSet	= 0.8 -- Time until ammo is set to the weapon
SWEP.Reload_TimeUntilFinished	= 1.8 -- How much time until the player can play idle animation, shoot, etc.
	-- Idle Settings ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.HasIdleAnimation			= true -- Does it have a idle animation?
SWEP.AnimTbl_Idle				= {ACT_VM_IDLE}
SWEP.NextIdle_Deploy			= 0.5 -- How much time until it plays the idle animation after the weapon gets deployed
SWEP.NextIdle_PrimaryAttack		= 0.1 -- How much time until it plays the idle animation after attacking(Primary)
---------------------------------------------------------------------------------------------------------------------------------------------
function SWEP:NPC_ServerNextFire()
	if (CLIENT) then return end
	if !self:IsValid() && !IsValid(self.Owner) && !self.Owner:IsValid() && !self.Owner:IsNPC() then return end
	if self:IsValid() && IsValid(self.Owner) && self.Owner:IsValid() && self.Owner:IsNPC() && self.Owner:GetActivity() == nil then return end

	self:RunWorldModelThink()
	self:CustomOnThink()
	self:CustomOnNPC_ServerThink()

	if self.Owner.HasDoneReloadAnimation == false && self.AlreadyPlayedNPCReloadSound == false && (VJ_IsCurrentAnimation(self.Owner,self.CurrentAnim_WeaponReload) or VJ_IsCurrentAnimation(self.Owner,self.CurrentAnim_ReloadBehindCover) or VJ_IsCurrentAnimation(self.Owner,self.NPC_ReloadAnimationTbl) or VJ_IsCurrentAnimation(self.Owner,self.NPC_ReloadAnimationTbl_Custom)) then
		self.Owner.NextThrowGrenadeT = self.Owner.NextThrowGrenadeT + 2
		self.Owner.HasDoneReloadAnimation = true
		--//self.Owner.IsReloadingWeapon = false
		self:CustomOnNPC_Reload()
		self.AlreadyPlayedNPCReloadSound = true
		if self.NPC_HasReloadSound == true then VJ_EmitSound(self.Owner,self.NPC_ReloadSound,self.NPC_ReloadSoundLevel) end
		timer.Simple(3,function() if IsValid(self) then self.AlreadyPlayedNPCReloadSound = false end end)
	end

	local function FireCode()
		self:NPCShoot_Primary(ShootPos,ShootDir)
		--//timer.Simple(0.1,function() if self:IsValid() && IsValid(self.Owner) && self.Owner:IsValid() && self.Owner:IsNPC() then self:NPCShoot_Primary(ShootPos,ShootDir) end end)
		--//timer.Simple(0.2,function() if self:IsValid() && IsValid(self.Owner) && self.Owner:IsValid() && self.Owner:IsNPC() then self:NPCShoot_Primary(ShootPos,ShootDir) end end)
		hook.Remove("Think", self)
		--//print(self.NPC_NextPrimaryFire)
		timer.Simple(self.NPC_NextPrimaryFire, function() hook.Add("Think",self,self.NPC_ServerNextFire) end)
		--//self.NPC_NextPrimaryFireT = CurTime() + self.NPC_NextPrimaryFire

	end


	if self:NPCAbleToShoot() == true then FireCode() end
end

function SWEP:PrimaryAttackEffects()
	local vjeffectmuz = EffectData()
	vjeffectmuz:SetOrigin(self.Owner:GetShootPos())
	vjeffectmuz:SetEntity(self.Weapon)
	vjeffectmuz:SetStart(self.Owner:GetShootPos())
	vjeffectmuz:SetNormal(self.Owner:GetAimVector())
	vjeffectmuz:SetAttachment(1)
	vjeffectmuz:SetMagnitude(0)
	util.Effect("",vjeffectmuz)
	
	if GetConVarNumber("vj_wep_nobulletshells") == 0 then
	if !self.Owner:IsPlayer() then
	local vjeffect = EffectData()
	vjeffect:SetEntity(self.Weapon)
	vjeffect:SetOrigin(self.Owner:GetShootPos())
	vjeffect:SetNormal(self.Owner:GetAimVector())
	vjeffect:SetAttachment(1)
	util.Effect("",vjeffect) end
	end

	if (SERVER) then
	if GetConVarNumber("vj_wep_nomuszzleflash") == 0 then
	local FireLight1 = ents.Create("light_dynamic")
	FireLight1:SetKeyValue("brightness", "2")
	if self.Owner:IsPlayer() then
	FireLight1:SetKeyValue("distance", "200") else FireLight1:SetKeyValue("distance", "150") end
	FireLight1:SetLocalPos(self.Owner:GetShootPos() +self:GetForward()*40 + self:GetUp()*-40)
	FireLight1:SetLocalAngles(self:GetAngles())
	FireLight1:Fire("Color", "0 31 225")
	FireLight1:SetParent(self)
	FireLight1:Spawn()
	FireLight1:Activate()
	FireLight1:Fire("TurnOn", "", 0)
	self:DeleteOnRemove(FireLight1)
	timer.Simple(0.07,function() if self:IsValid() then FireLight1:Remove() end end)
	end
 end
end
---------------------------------------------------------------------------------------------------------------------------------------------
function SWEP:FireAnimationEvent(pos,ang,event,options)
	/*local vjeffect = EffectData()
	vjeffect:SetEntity(self.Weapon)
	vjeffect:SetOrigin(self.Owner:GetShootPos())
	vjeffect:SetNormal(self.Owner:GetAimVector())
	vjeffect:SetAttachment(2)
	util.Effect("",vjeffect)*/
	
	//print(event)
	//if GetConVarNumber("vj_wep_nomuszzleflash") == 1 then
	if event == 5001 then return true end 
	//end
	
	if GetConVarNumber("vj_wep_nobulletshells") == 1 then
	if event == 20 then 
		return true end 
	end
end

function SWEP:PrimaryAttack(ShootPos,ShootDir)
	//if self.Owner:KeyDown(IN_RELOAD) then return end
	//self.Owner:SetFOV( 45, 0.3 )
	self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	if self.Reloading == true then return end
	if self.Owner:IsNPC() && self.Owner:GetEnemy() == nil then return end
	if self.Owner:IsPlayer() && self.Primary.AllowFireInWater == false && self.Owner:WaterLevel() == 3 && self.Reloading == false then 
	self.Weapon:EmitSound(Sound(self.DryFireSound),50,math.random(90,100)) return end
	if (!self:CanPrimaryAttack()) then return end
	if self:Clip1() <= 0 && self.Reloading == false then
	self.Weapon:EmitSound(Sound(self.DryFireSound),50,math.random(90,100)) return end
	self:CustomOnPrimaryAttack_BeforeShoot()
	if (SERVER) then
		sound.Play(Sound(self.Primary.Sound),self:GetPos(),80,math.random(90,100))
		if self.Primary.HasDistantSound == true then
		sound.Play(Sound(self.Primary.DistantSound),self:GetPos(),self.Primary.DistantSoundLevel,math.random(self.Primary.DistantSoundPitch1,self.Primary.DistantSoundPitch2),self.Primary.DistantSoundVolume)
		end
	end
	//self.Weapon:EmitSound(Sound(self.Primary.Sound),80,self.Primary.SoundPitch)
	if self.Primary.DisableBulletCode == false then
	local bullet = {}
		bullet.Num = self.Primary.NumberOfShots
		bullet.Src = self.Owner:GetShootPos()
		bullet.Dir = self.Owner:GetAimVector()
			/*bullet.Callback = function(attacker, tr, dmginfo)
			local laserhit = EffectData()
			laserhit:SetOrigin(tr.HitPos)
			laserhit:SetNormal(tr.HitNormal)
			laserhit:SetScale(80)
			util.Effect("VJ_Small_Explosion1", laserhit)
			
			bullet.Callback = function(attacker, tr, dmginfo)
			local laserhit = EffectData()
			laserhit:SetOrigin(tr.HitPos)
			laserhit:SetNormal(tr.HitNormal)
			laserhit:SetScale(25)
			util.Effect("AR2Impact", laserhit)
			end*/
			//tr.HitPos:Ignite(8,0)
			//return true end
		if self.Owner:IsPlayer() then
			bullet.Spread = Vector((self.Primary.Cone /60)/4,(self.Primary.Cone /60)/4,0)
		end
		bullet.Tracer = self.Primary.Tracer
		bullet.TracerName = self.Primary.TracerType
		bullet.Force = self.Primary.Force
		if self.Owner:IsPlayer() then
			if self.Primary.UseNegativePlayerDamage == true then
			bullet.Damage = self.Primary.Damage -self.Primary.PlayerDamage else
			bullet.Damage = self.Primary.Damage *self.Primary.PlayerDamage end
		else
			bullet.Damage = self.Primary.Damage
		end
		bullet.AmmoType = self.Primary.Ammo
	self.Owner:FireBullets(bullet)
	if self.Owner:IsNPC() && self.Owner.IsVJBaseSNPC == true then 
	
	

	
	self.firecontrol_firing = 1
	
	timer.Simple(1.0,function()
	
	if self:IsValid() then
	if self.firecontrol_firing == 1 then 
	
	--timer.Simple(1.0,function()
	
	--if self:IsValid() then
	self.firecontrol_firing = 0 
	--end
	--end)
	
	end
	end
	end)
	
	
	local function Timer_Bulletfirecontrol()
	
	if self != NULL then
	if self:IsValid() then
	if self.firecontrol_firing == 0 then 
	self.firecontrol_shotfired = 0 end
	end
	end
	end
	
	
	timer.Create( "timer_bullet_attack"..self.Owner:EntIndex(), 1.5, 1, Timer_Bulletfirecontrol )
		
	self.firecontrol_shotfired = self.firecontrol_shotfired + 1 
	
	if self.firecontrol_shotfired >= (3 + (math.random(0,2))) && (self.Owner:GetEnemy()) && (self.Owner:EyePos():Distance(self.Owner:GetEnemy():EyePos()) > 3000) then
	
	self.firecontrol_longrange = 1
	
	timer.Simple((0.85 + (math.random(0,6))*0.1),function()
	
	if self:IsValid() then
	self.firecontrol_longrange = 0
	self.firecontrol_shotfired = 0
	end
	end)
	end
	
	if self.firecontrol_shotfired >= (8 + (math.random(0,5))) && (self.Owner:GetEnemy()) && (self.Owner:EyePos():Distance(self.Owner:GetEnemy():EyePos()) <= 3000) && (self.Owner:EyePos():Distance(self.Owner:GetEnemy():EyePos()) > 1200) then
	
	self.firecontrol_mediumrange = 1
	
	timer.Simple((0.7 + (math.random(0,4))*0.1),function()
	if self:IsValid() then
	self.firecontrol_mediumrange = 0
	self.firecontrol_shotfired = 0
	end
	end)
	end
	
	end
	else
	if self.Owner:IsNPC() && self.Owner.IsVJBaseSNPC == true then
		self.Owner.Weapon_ShotsSinceLastReload = self.Owner.Weapon_ShotsSinceLastReload + 1
		end
	end
	if GetConVarNumber("vj_wep_nomuszzleflash") == 0 then
	self.Owner:MuzzleFlash() end
	self:PrimaryAttackEffects()
	if self.Owner:IsPlayer() then
	self:ShootEffects("ToolTracer")
	self.Weapon:SendWeaponAnim(VJ_PICKRANDOMTABLE(self.AnimTbl_PrimaryFire))
	self.Owner:SetAnimation(PLAYER_ATTACK1)
	self.Owner:ViewPunch(Angle(-self.Primary.Recoil,0,0)) end
	if !self.Owner:IsNPC() then
		self:TakePrimaryAmmo(self.Primary.TakeAmmo)
	end
	self:CustomOnPrimaryAttack_AfterShoot()
	//self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	timer.Simple(self.NextIdle_PrimaryAttack,function() if self:IsValid() then self:DoIdleAnimation() end end)
end

function SWEP:OnRemove()
	--if self.ownerindex != NIL || self.ownerindex != NULL then
	--timer.Destroy("timer_bullet_attack"..self.ownerindex) end
	self:StopParticles()
	self.Deleted = true
end

---------------------------------------------------------------------------------------------------------------------------------------------
function SWEP:NPCShoot_Primary(ShootPos,ShootDir)
	if CurTime() > self.NPC_NextPrimaryFireT then
	self.NPC_NextPrimaryFireT = CurTime() + self.NPC_NextPrimaryFire
	//self:SetClip1(self:Clip1() -1)
	if (!self:IsValid()) or (!self.Owner:IsValid()) then return end
	if IsValid(self) && IsValid(self.Owner) then
	self.ownerindex = self.Owner:EntIndex() end
	
	if (!self.Owner:GetEnemy()) then return end
	if self.Owner.IsVJBaseSNPC == true then
		self.Owner.Weapon_TimeSinceLastShot = 0
		self.Owner.NextWeaponAttackAimPoseParametersReset = CurTime() + 1
		self.Owner:WeaponAimPoseParameters()
	end
	if self.firecontrol_longrange != 1 && self.firecontrol_mediumrange != 1 then
	timer.Simple(self.NPC_TimeUntilFire,function()
	if IsValid(self) && IsValid(self.Owner) then
		self:PrimaryAttack()
		end
	end)
	end
 end
end

function SWEP:CustomOnInitialize() 

self.firecontrol_longrange = 0
self.firecontrol_mediumrange = 0
self.firecontrol_shortrange = 0
self.firecontrol_shotfired = 0
self.firecontrol_firing = 0
if IsValid(self) && IsValid(self.Owner) then
self.ownerindex = self.Owner:EntIndex()
end

	timer.Simple(0.05, function()
	
	if self:IsValid() then
	if self.Owner != nil && self.Owner:IsValid() then
	if (self.Owner:IsNPC() == true) && self.Owner.IsVJBaseSNPC == true && self.Owner.ShootDistance != nil then
	if self.Owner.ShootDistance >= 2900 then
	
	self.Owner.ShootDistance = self.Owner.ShootDistance - 580
	self.Owner.Weapon_FiringDistanceFar = self.Owner.ShootDistance
	self.Owner.DropWeaponOnDeath = false
	end
	end
	end
	end
	
	end)

end