if (!file.Exists("autorun/vj_base_autorun.lua","LUA")) then return end
---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.Base 						= "weapon_vj_base"
SWEP.PrintName					= "Droideka Blaster"
SWEP.Author 					= "Vac"
SWEP.Contact					= "http://steamcommunity.com/groups/vrejgaming"
SWEP.Purpose					= "This weapon is made for NPCs"
SWEP.Instructions				= "Controls are like a regular weapon."
SWEP.Category					= "VJ Base"
	-- Main Settings ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.ViewModel					= ""
SWEP.WorldModel					= ""
SWEP.HoldType 					= "pistol"
SWEP.ViewModelFlip				= false -- Flip the model? Usally used for CS:S models
SWEP.Spawnable					= false
SWEP.AdminSpawnable				= false
SWEP.MadeForNPCsOnly 			= true
	-- Primary Fire ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.Primary.Damage				= 10 -- Damage
SWEP.Primary.Force				= 5 -- Force applied on the object the bullet hits
SWEP.Primary.ClipSize			= 99999 -- Max amount of bullets per clip
SWEP.NPC_NextPrimaryFire 		= 4 -- Next time it can use primary fire
SWEP.NPC_TimeUntilFire 			= 4 
SWEP.Primary.TakeAmmo			= 0
SWEP.Primary.Ammo				= "SMG1" -- Ammo type
SWEP.Primary.Sound				= {"impact/dwarf_spider_fire.wav"}
SWEP.PrimaryEffects_DynamicLightColor = Color(255, 0, 0)
SWEP.WorldModel_Invisible 		= true -- Should the world model be invisible?
SWEP.NPC_BulletSpawnAttachmentOwner = "Gun" -- The attachment that the bullet spawns on, leave empty for base to decide!
SWEP.PrimaryEffects_SpawnShells = false
SWEP.PrimaryEffects_MuzzleFlash = false
SWEP.NPC_HasSecondaryFire = false

function SWEP:CustomOnPrimaryAttack_BeforeShoot()
	if (CLIENT) then return end
	
	local bullet = ents.Create("obj_guard_shot")
	bullet:SetPos(self.Owner:GetAttachment(1).Pos)
	bullet:Activate()
	bullet:Spawn()
	bullet:PointAtEntity(self.Owner:GetEnemy())
	local phys = bullet:GetPhysicsObject()
	if phys:IsValid() then
		phys:SetVelocity(bullet:GetForward()*4000)
	end
end