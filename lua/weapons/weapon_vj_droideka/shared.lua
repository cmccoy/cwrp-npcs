if (!file.Exists("autorun/vj_base_autorun.lua","LUA")) then return end
---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.Base 						= "weapon_vj_base"
SWEP.PrintName					= "Droideka Blaster"
SWEP.Author 					= "Vac"
SWEP.Contact					= "http://steamcommunity.com/groups/vrejgaming"
SWEP.Purpose					= "This weapon is made for NPCs"
SWEP.Instructions				= "Controls are like a regular weapon."
SWEP.Category					= "VJ Base"
	-- Main Settings ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.ViewModel					= ""
SWEP.WorldModel					= ""
SWEP.HoldType 					= "pistol"
SWEP.ViewModelFlip				= false -- Flip the model? Usally used for CS:S models
SWEP.Spawnable					= false
SWEP.AdminSpawnable				= false
SWEP.MadeForNPCsOnly 			= true
	-- Primary Fire ---------------------------------------------------------------------------------------------------------------------------------------------
SWEP.Primary.Damage				= 10 -- Damage
SWEP.Primary.Force				= 5 -- Force applied on the object the bullet hits
SWEP.Primary.ClipSize			= 99999 -- Max amount of bullets per clip
SWEP.NPC_NextPrimaryFire 		= 0.1 -- Next time it can use primary fire
SWEP.NPC_TimeUntilFire 			= 0.1 
SWEP.Primary.TakeAmmo			= 0
SWEP.Primary.TracerType	 		= "effect_sw_laser_red"
SWEP.Primary.Automatic			= true -- Is it automatic?
SWEP.Primary.Ammo				= "SMG1" -- Ammo type
SWEP.Primary.Sound				= {"weapons/1misc_guns/wpn_fg_rg_droideka_blaster.ogg"}
SWEP.PrimaryEffects_DynamicLightColor = Color(255, 0, 0)
SWEP.WorldModel_Invisible 		= true -- Should the world model be invisible?
SWEP.NPC_BulletSpawnAttachmentOwner = "Left_Gun" -- The attachment that the bullet spawns on, leave empty for base to decide!
SWEP.PrimaryEffects_SpawnShells = false
SWEP.PrimaryEffects_MuzzleFlash = false
SWEP.NPC_HasSecondaryFire = false

local alternate = true
function SWEP:CustomOnPrimaryAttack_BeforeShoot() 
	if alternate then
		self.NPC_BulletSpawnAttachmentOwner = "Left_Gun"
		alternate = false
	else
		self.NPC_BulletSpawnAttachmentOwner = "Right_Gun"
		alternate = true
	end
end

function SWEP:DoImpactEffect( tr, dmgtype )
	if( tr.HitSky ) then return true; end
	
	local effectdata = EffectData()
	effectdata:SetOrigin( tr.HitPos )
	effectdata:SetScale( 1 )
	effectdata:SetMagnitude( 2 ) 
	util.Effect( "Sparks", effectdata, true, true )
	util.Decal( "fadingscorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal );

return true	
end