/*--------------------------------------------------
	=============== Autorun File ===============
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
--------------------------------------------------*/
------------------ Addon Information ------------------
local PublicAddonName = "Star Wars SNPCs - Battle Droids"
local AddonName = "Star Wars - Battle Droids"
local AddonType = "SNPC"
local AutorunFile = "autorun/vj_swbd_autorun.lua"
-------------------------------------------------------

	include('autorun/vj_controls.lua')

	local vCat = "Star Wars"

	-- Combine
	VJ.AddNPC_HUMAN("Standard Battle Droid","npc_vj_swbd_standard",{"weapon_vj_blaster"},vCat)
	VJ.AddNPC_HUMAN("Geonosis Battle Droid","npc_vj_swbd_genosis",{"weapon_vj_blaster"},vCat)
	VJ.AddNPC_HUMAN("Training Battle Droid","npc_vj_swbd_training",{"weapon_vj_blaster"},vCat)
	VJ.AddNPC_HUMAN("Commander Battle Droid","npc_vj_swbd_commander",{"weapon_vj_blaster"},vCat)
	VJ.AddNPC_HUMAN("Tactical Droid","npc_vj_swbd_tactical",{"weapon_vj_blaster"},vCat)
	VJ.AddNPC_HUMAN("Commando Droid","npc_vj_swbd_commando",{"weapon_vj_blaster"},vCat)
	VJ.AddNPC_HUMAN("Super Battle Droid","npc_vj_swbd_super",{},vCat)
	VJ.AddNPC_HUMAN("Super Battle Droid Rocket","npc_vj_swbd_super_rocket",{},vCat)
	VJ.AddNPC_HUMAN("Droideka","npc_vj_swbd_droideka",{},vCat)
	VJ.AddNPC_HUMAN("Dwarf Spider Droid","npc_vj_swbd_dwarf_spider",{},vCat)
	VJ.AddNPC("Tri Droid","npc_vj_swbd_tri_droid",vCat)
	VJ.AddNPC("Rancor Huge","npc_vj_rancor_huge",vCat)
	VJ.AddNPC("Rancor Medium","npc_vj_rancor_medium",vCat)
	VJ.AddNPC("AAT","npc_aat_body",vCat)


	VJ.AddNPCWeapon("VJ_B2-Blaster","weapon_vj_b2_blaster")
	VJ.AddNPCWeapon("VJ_B2-Rocket","weapon_vj_b2_rocket")
	VJ.AddNPCWeapon("Droideka_gun","weapon_vj_droideka")

	-- ConVars --
	VJ.AddConVar("vj_swbd_standard_h",75)
	VJ.AddConVar("vj_swbd_standard_d",10)
	
	VJ.AddConVar("vj_swbd_genosis_h",75)
	VJ.AddConVar("vj_swbd_genosis_d",10)
	
	VJ.AddConVar("vj_swbd_training_h",50)
	VJ.AddConVar("vj_swbd_training_d",10)
	
	VJ.AddConVar("vj_swbd_commander_h",120)
	VJ.AddConVar("vj_swbd_commander_d",15)
	
	VJ.AddConVar("vj_swbd_tactical_h",150)
	VJ.AddConVar("vj_swbd_tactical_d",20)
	
	VJ.AddConVar("vj_swbd_commando_h",250)
	VJ.AddConVar("vj_swbd_commando_d",25)
	
	-- Precache Models --
	util.PrecacheModel("models/hfg/starwars/droids/commandodroid/head.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/commandodroid/left_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/commandodroid/left_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/commandodroid/right_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/commandodroid/right_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/commandodroid/torso.mdl")
	
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_commander/head.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_commander/left_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_commander/left_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_commander/right_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_commander/right_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_commander/torso.mdl")
	
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_geo/head.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_geo/left_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_geo/left_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_geo/right_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_geo/right_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_geo/torso.mdl")
	
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_infantry/head.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_infantry/left_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_infantry/left_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_infantry/right_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_infantry/right_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_infantry/torso.mdl")
	
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_training/head.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_training/left_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_training/left_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_training/right_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_training/right_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_bdroid_training/torso.mdl")
	
	util.PrecacheModel("models/hfg/starwars/droids/sw_droid_tactical/head.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_droid_tactical/left_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_droid_tactical/left_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_droid_tactical/right_arm.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_droid_tactical/right_leg.mdl")
	util.PrecacheModel("models/hfg/starwars/droids/sw_droid_tactical/torso.mdl")

	util.PrecacheModel("models/tfa/comm/gg/npc_cit_sw_droid_b2.mdl")
	util.PrecacheModel("models/tfa/comm/gg/npc_cit_sw_droid_b2_gunvariant.mdl")

	util.PrecacheModel("models/props/ig/rancor.mdl")

	util.PrecacheModel("models/combine_guard.mdl")
	
-- !!!!!! DON'T TOUCH ANYTHING BELOW THIS !!!!!! -------------------------------------------------------------------------------------------------------------------------
	AddCSLuaFile(AutorunFile)
	VJ.AddAddonProperty(AddonName,AddonType)