/*--------------------------------------------------
	=============== Half-life 2+ Spawn ===============
	*** Copyright (c) 2012-2015 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
INFO: Used to load spawns for Half-life 2+
--------------------------------------------------*/
if (!file.Exists("autorun/vj_base_autorun.lua","LUA")) then return end
include('autorun/vj_controls.lua')

local vCat = "VJ Base Combine"
VJ.AddNPC_HUMAN("Combine Super Soldier","npc_combine_super_soldier",{"weapon_vj_hl2_ar2"},vCat)
VJ.AddNPC_HUMAN("Combine Soldier","npc_combine_soldier",{"weapon_vj_hl2_ar2"},vCat)
VJ.AddNPC_HUMAN("Combine Grenadier","npc_combine_grenadier",{"weapon_vj_hl2_ar2"},vCat)
VJ.AddNPC("Combine Guard","npc_combine_guard",vCat)



-- Weapons
VJ.AddNPCWeapon("VJ - AR2", "weapon_vj_hl2_ar2")
VJ.AddNPCWeapon("VJ - Shotgun(BMS)", "weapon_vj_bms_shotgun")
VJ.AddNPCWeapon("VJ - AR2+", "weapon_vj_hl2b_ar2+")