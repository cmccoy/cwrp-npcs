/*--------------------------------------------------
	=============== Half-life 2+ Autorun ===============
	*** Copyright (c) 2012-2015 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
INFO: Used to load autorun file for Half-life 2+
--------------------------------------------------*/
-- Addon Information(Important!):
	local PublicAddonName = "Half-life 2+"
	local AddonName = "Half-life 2+"
	local AddonType = "SNPC"
-- Don't edit anything below this! ------------------------------------------------

local VJExists = "lua/autorun/vj_base_autorun.lua"

if( file.Exists( VJExists, "GAME" ) ) then
	include('autorun/vj_controls.lua')
	AddCSLuaFile("autorun/vj_hl2p_autorun.lua")
	AddCSLuaFile("autorun/vj_hl2p_convar.lua")
	AddCSLuaFile("autorun/vj_hl2p_spawn.lua")
	VJ.AddAddonProperty(AddonName,AddonType)
end