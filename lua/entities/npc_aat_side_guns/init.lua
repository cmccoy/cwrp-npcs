AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2016 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------
*/
ENT.Model = "models/npc/de_sdkfz222_des_turret.mdl" -- Leave empty if using more than one model 
ENT.IsVJBaseSNPC_Tank = true -- Is it a VJ Base tank SNPC?
ENT.HasSetSolid = false -- set to false to disable SetSolid
ENT.VJ_IsHugeMonster = false -- Is this a huge monster?
ENT.StartHealth = 0
---------------------------------------------------------------------------------------------------------------------------------------------
ENT.VJ_NPC_Class = {"CLASS_GERMAN"} 
ENT.SquadName = "enemy_german" -- Squad name, console error will happen if two groups that are enemy and try to squad!
ENT.PlayerFriendly = false -- Makes the SNPC friendly to the player and HL2 Resistance

-- Tank Base
ENT.Tank_AngleDiffuseNumber = 0
ENT.Tank_UseNegativeAngleDiffuseNumber = false -- true
ENT.Tank_AngleDiffuseGeneralNumber = 3.0
ENT.Tank_UsesRightAngles = false --true
ENT.ShellSpawnPos = Vector(225,0,20)
ENT.ShellLightPos = Vector(225,0,12)
ENT.ShellMuzzlePos = Vector(225,0,12)
ENT.ShellParticlePos1 = Vector(225,0,18)
ENT.ShellParticlePos2 = Vector(225,0,18)

ENT.MG1SpawnPos = Vector(66,12.2,6.5)
ENT.MG1MuzzlePos = Vector(42,12.2,4.5)
ENT.MG1SpawnPos_muzzle = Vector(42,12.2,4.5)
--ENT.ShellSpawnPos = Vector(0,-70,15)
--ENT.ShellLightPos = Vector(-75,0,0)
--ENT.ShellMuzzlePos = Vector(0,-75,12)
--ENT.ShellParticlePos1 = Vector(0,-75,18)
--ENT.ShellParticlePos2 = Vector(0,-75,18)
ENT.Tank_SeeFar = 3800 -- If the enemy is higher than this number, than don't shoot!
ENT.Tank_SeeLimit = 4200 -- How far can it see?
ENT.ShellShootUp = 35 --10
ENT.Tank_NotFacingTargetShootPos = 280
ENT.Tank_SeeClose = 450 -- If the enemy is closer than this number, than don't shoot!

ENT.Tank_MG_Damage = 9
ENT.Tank_MG_Cone = 7
ENT.MGShootUp = -18
ENT.MG1_Sound = "sdkfz222/tank_mg34_shoot.wav"

util.AddNetworkString("vj_mili_tiger_shooteffects")

---------------------------------------------------------------------------------------------------------------------------------------------







function ENT:CustomOnSchedule()
	if self.Dead == true then return end

	if self:GetEnemy() == nil then
		if self.Tank_ResetedEnemy == false then
		self.Tank_ResetedEnemy = true
		self:ResetEnemy() end
		//self:FindEnemySphere()
	else
		self.Tank_ResetedEnemy = false
		EnemyPos = self:GetEnemy():GetPos()
		EnemyPosToSelf = self:GetPos():Distance(EnemyPos)
		if EnemyPosToSelf > self.Tank_SeeLimit then -- If more than this, Don't fire!
			self.Tank_Status = 1
		elseif EnemyPosToSelf < self.Tank_SeeFar && EnemyPosToSelf > self.Tank_SeeClose then -- Between this two numbers than fire!
			self.Tank_Status = 0
		else
			self.Tank_Status = 1
		end
	end
end


function ENT:RangeAttack_Base()
if self.Tank_ProperHeightShoot == false then return end

local function Timer_ShellAttack()
	self:RangeAttack_Shell()
	self.FiringShell = false
end

if self.Tank_ShellReady == false && self.Tank_ShellRecharging == 0 then	

	self.Tank_ShellReady = true
end

if self.Dead == false then
	timer.Create( "timer_shell_attack"..self.Entity:EntIndex(), 1, 1, Timer_ShellAttack ) 
	
	end
	
	
	--self:RangeAttack_MG1()
	
	
end


function ENT:CustomInitialize()

	self:SetNoDraw(true)
	self.Tank_ShellRecharging = 0
	self.Tank_MGRecharging = 0
	self.Tank_MGbulletfired = 0
	
	
    self.Whee = ents.Create("prop_dynamic")
    self.Whee:SetModel("models/weapons/german/weapon_de_eierhandgranate.mdl")
	self.Whee:SetPos(self:LocalToWorld(self.MG1SpawnPos))
	self.Whee:SetParent( self )
	self.Whee:SetAngles( self:GetAngles() )
	self.Whee:Spawn()
	self:DeleteOnRemove(self.Whee)
	self.Whee:SetNoDraw(true)
	self.Whee.Owner = self
	
	
	
    /*
	--self.Wheel = ents.Create("prop_dynamic")
   -- self.Wheel:SetModel("models/beat the zombie/WoT/German/tiger_i_gun.mdl")
	--self.Wheel:SetPos( self:GetPos() + self:GetForward() * 45 + self:GetRight() * 0 + self:GetUp() * 15 )
	--self.Wheel:SetParent( self )
	--self.Wheel:SetSkin( self:GetSkin() )
	---self.Wheel:SetAngles( self:GetAngles() )
	---self.Wheel:Spawn()
   --- self.Wheel:SetBodygroup(0,4)
	*/
end


function ENT:CustomOnThink_AIEnabled()
	if self.Dead == true then return end
	//print(self:GetEnemy())
	//if self.Tank_ShellReady == true then print("Tank_ShellReady = true") else print("Tank_ShellReady = false") end
	//if self.Tank_FacingTarget == true then print("Tank_FacingTarget = true") else print("Tank_FacingTarget = false") end
	//if self.FiringShell == true then print("FiringShell = true") else print("FiringShell = false") end

	//self:FindEnemySphere()
	//print(self:GetEnemy())


	if self:GetParent():GetEnemy() != nil && self:GetEnemy() == nil then
	self:SetEnemy(self:GetParent():GetEnemy())
	end
	
	self:CustomOnSchedule()
	
	if self.Tank_FacingTarget == false then self.FiringShell = false end
	if self.Tank_ShellReady == false then self.FiringShell = false end
	if self.Tank_Status == 0 then
		if self:GetEnemy() == nil then
		self.Tank_Status = 1
		self.Tank_GunnerIsTurning = false
	else
	-- x = Forward | y = Right | z = Up
	
	--local fucktraces = { start = self:GetPos(), endpos = self:GetPos() + self:GetUp()*-85, filter = self }
	--local tr = util.TraceEntity( fucktraces, self ) 
	
	local fucktraces1 = { start = self:GetParent():GetPos(), endpos = self:GetParent():GetPos() + self:GetParent():GetUp()*-15, filter = self:GetParent() }
	local tr1 = util.TraceEntity( fucktraces1, self:GetParent() )
	
	local phys2 = self:GetParent():GetPhysicsObject()
	local Angle_Enemy = (self:GetEnemy():GetPos() - self:GetPos() /*+ Vector(80,0,80)*/):Angle()
	local Angle_Current = self:GetAngles()
	if self.Tank_UseNegativeAngleDiffuseNumber == true then
	Angle_Diffuse = self:AngleDiffuse(Angle_Enemy.y,Angle_Current.y-self.Tank_AngleDiffuseNumber) else -- Cannon looking direction
	Angle_Diffuse = self:AngleDiffuse(Angle_Enemy.y,Angle_Current.y+self.Tank_AngleDiffuseNumber) end -- Cannon looking direction
	local Heigh_Ratio = (self:GetEnemy():GetPos().z - self:GetPos().z ) / self:GetPos():Distance(Vector(self:GetEnemy():GetPos().x,self:GetEnemy():GetPos().y,self:GetPos().z))
	-- ^^^ How high it can shoot ^^^ --
	if math.abs(Heigh_Ratio) < 0.15 then self.Tank_ProperHeightShoot = true else self.Tank_ProperHeightShoot = false end
	self.Tank_GunnerIsTurning = false
	if math.abs(Angle_Diffuse) < self.Tank_AngleDiffuseGeneralNumber && self.FiringShell == false && math.abs(Heigh_Ratio) < 0.15 && self:GetPos():Distance(self:GetEnemy():GetPos()) > self.Tank_SeeClose then
	-- If the diffuse and the height and the enemy distance is higher than the self.Tank_SeeClose than shoot!
		self.Tank_GunnerIsTurning = false
		self.FiringShell = true
		self.Tank_FacingTarget = true
		if self:Visible(self:GetEnemy()) then
		if GetConVarNumber("vj_npc_norange") == 0 then
		--self:RangeAttack_Base()
		timer.Simple(0.50, function()
		if self:IsValid() then
		if self.Dead != true then

		self:RangeAttack_MG1() 

		end
		end
		end)
		
		
		
		end end
	elseif Angle_Diffuse > self.Tank_AngleDiffuseGeneralNumber then
		//self:SetLocalAngles( self:GetLocalAngles() + Angle(0,2,0))
		if (self:GetParent():IsValid()) && (self:GetParent().Tank_Status != nil) then
		if self:GetParent().Tank_Status == 1 then
		
		if phys2:IsValid() then
		--if phys2:GetVelocity():Length() <= 10 then 
		
		local tracedata11 = {}
		tracedata11.start = self:GetParent():GetPos()
		tracedata11.endpos = self:GetParent():GetPos() + Vector(0,0,-65)
		tracedata11.filter = {self,self:GetParent()}
		local tr11 = util.TraceLine(tracedata11)
		
		
		
		if ( tr11.HitWorld ) then
		
		--self:GetParent():SetAngles( self:GetParent():GetAngles() + Angle(0,2,0))
		//self:SetLocalAngles( self:GetParent():GetAngles() + Angle(0,0,0))
		
		--self:GetParent():SetLocalAngles( self:GetParent():GetLocalAngles() + Angle(0,2,0))
		--phys2:SetAngles(self:GetParent():GetAngles())
		
		end
		--end
		end
		
		end
		end
		
		self.Tank_GunnerIsTurning = true
		self.Tank_FacingTarget = false
		self.FiringShell = false
	elseif Angle_Diffuse < -self.Tank_AngleDiffuseGeneralNumber then
		//self:SetLocalAngles( self:GetLocalAngles() + Angle(0,-2,0))
		if (self:GetParent():IsValid()) && (self:GetParent().Tank_Status != nil) then
		if self:GetParent().Tank_Status == 1 then
		
		if phys2:IsValid() then
		--if phys2:GetVelocity():Length() <= 10 then 
		
		--local fucktraces12 = { start = self:GetPos(), endpos = self:GetPos() + self:GetUp()*-185, filter = self }
		--local tr12 = util.TraceEntity( fucktraces, self )

		local tracedata12 = {}
		tracedata12.start = self:GetParent():GetPos()
		tracedata12.endpos = self:GetParent():GetPos() + Vector(0,0,-65)
		tracedata12.filter = {self,self:GetParent()}
		local tr12 = util.TraceLine(tracedata12)
		
		if ( tr12.HitWorld ) then
		
		--self:GetParent():SetAngles( self:GetParent():GetAngles() + Angle(0,-2,0))
		//self:SetLocalAngles( self:GetParent():GetAngles() + Angle(0,0,0))
		
		
		--self:GetParent():SetLocalAngles( self:GetParent():GetLocalAngles() + Angle(0,-2,0))
		--phys2:SetAngles(self:GetParent():GetAngles())
		
		--end
		end
		end
		
		end 
		end
		self.Tank_GunnerIsTurning = true
		self.Tank_FacingTarget = false
		self.FiringShell = false
	end
  end
 end
end

function ENT:RangeAttack_MG1()
	if self.Dead == true then return end
	if self.Tank_MGRecharging == 1 then return end
	
	if self.Tank_FacingTarget == true && IsValid(self:GetEnemy()) && self:GetEnemy() != NULL && self:GetEnemy() != nil then
	if self:Visible(self:GetEnemy()) || self.Tank_MGRecharging != 1 then

	local bulletshootpos = (self:GetEnemy():GetPos()-self:LocalToWorld(self.MG1SpawnPos) + self:GetUp()*-self.MGShootUp + self:GetRight()*0)
	local bulletsrc = self.Whee:GetPos() --self:LocalToWorld(self.MG1SpawnPos) --self:LocalToWorld(self.MG1SpawnPos)
	
	local bulletmuzzlepos = self:LocalToWorld(self.MG1MuzzlePos)
	
	if (math.Rand(0,8) >= 5 ) then 
	
	bulletshootpos = (self:GetEnemy():GetPos()-self:LocalToWorld(self.MG1SpawnPos) + self:GetUp()*-(math.random(-50,20)) + self:GetRight()*-(math.random(-35,35)))
	
	end
	
	if (SERVER) then
		sound.Play(Sound(self.MG1_Sound),self:GetPos(),100,math.random(90,110))
	end

	
	
	local flash = ents.Create("env_muzzleflash")
	flash:SetPos(self:LocalToWorld(self.MG1MuzzlePos))
	flash:SetKeyValue("scale","0.6")

	if self.Tank_UsesRightAngles == true then
	flash:SetKeyValue("angles",tostring(self:GetRight():Angle())) else
	flash:SetKeyValue("angles",tostring((self:GetForward()*1):Angle())) end
	flash:Fire( "SetParent", self, 0 )
	flash:Fire( "Fire", 0, 0 )
	
	self.FireLightmg2 = ents.Create("light_dynamic")
	self.FireLightmg2:SetKeyValue("brightness", "2")
	self.FireLightmg2:SetKeyValue("distance", "150")
	self.FireLightmg2:SetPos(self:LocalToWorld(self.MG1MuzzlePos))
	self.FireLightmg2:SetLocalAngles(self:GetAngles())
	self.FireLightmg2:Fire("Color", "255 150 60")
	self.FireLightmg2:SetParent(self)
	self.FireLightmg2:Spawn()
	self.FireLightmg2:Activate()
	self.FireLightmg2:Fire("TurnOn", "", 0)
	self:DeleteOnRemove(self.FireLightmg2)
	timer.Simple(0.03,function() 
	
	if self.Dead == false then 
	self.FireLightmg2:Remove() 
	end 
	
	end)
	

	
	local bullet = {}
		bullet.Attacker = self
		bullet.Num = 1
		bullet.Src = bulletsrc
		bullet.Dir = bulletshootpos
		bullet.Tracer = 1
		bullet.TracerName = "Tracer"
		bullet.Force = 5
		bullet.Damage = self.Tank_MG_Damage
		bullet.AmmoType = "SMG1"
		bullet.Spread = Vector((self.Tank_MG_Cone /60)/4,(self.Tank_MG_Cone /60)/4,0)
		--bullet.Callback = function(attacker, trace, dmginfo)
		--	dmginfo:SetDamageType(DMG_BULLET)
		--end
		self.Whee:FireBullets(bullet)
		
		self.Tank_MGbulletfired = self.Tank_MGbulletfired + 1
		
		
		if self.Tank_MGbulletfired >= 50 then
		
		self.Tank_MGRecharging = 1
		self.Tank_MGbulletfired = 0
		timer.Simple(5.5,function() if self.Dead == false then self.Tank_MGRecharging = 0 end end)

		end
		
		end
		end
end



function ENT:RangeAttack_Shell()
	if self.Dead == true then return end
	if self.Tank_ShellRecharging == 1 then return end
	if self.Dead == false then 
	if GetConVarNumber("ai_disabled") == 0 then
	if self.Tank_ProperHeightShoot == false then return end
	if IsValid(self:GetEnemy()) && self:GetEnemy() != NULL && self:GetEnemy() != nil then
	if self:Visible(self:GetEnemy()) then

		self.Tank_ShellReady = false
		self.FiringShell = false
		self.Tank_ShellRecharging = 1
		timer.Simple(4.50, function() if self.Dead == false then self.Tank_ShellRecharging = 0 end end)
		else
		self.Tank_ShellReady = false
		self.FiringShell = false
		self.Tank_FacingTarget = false
	end
   end
  end
 end
end









/*-----------------------------------------------
	*** Copyright (c) 2012-2016 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/