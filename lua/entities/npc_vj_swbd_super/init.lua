AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = {"models/tfa/comm/gg/npc_cit_sw_droid_b2.mdl"} -- The game will pick a random model from the table when the SNPC is spawned | Add as many as you want 
ENT.StartHealth = 400
ENT.HullType = HULL_HUMAN
---------------------------------------------------------------------------------------------------------------------------------------------
ENT.VJ_NPC_Class = {"CLASS_STARWARS_CIS"} -- NPCs with the same class with be allied to each other
ENT.HasMeleeAttack = true -- Should the SNPC have a melee attack?
ENT.MeleeAttackDamage = 50
ENT.FootStepTimeRun = 0.25 -- Next foot step sound when it is running
ENT.FootStepTimeWalk = 0.5 -- Next foot step sound when it is walking
ENT.DropWeaponOnDeath = false
ENT.IdleAlwaysWander = true
ENT.CanOpenDoors = true
ENT.DisableWandering = false
	-- ====== Sound File Paths ====== --
-- Leave blank if you don't want any sounds to play
ENT.SoundTbl_FootStep = {"vj_battledroid/step1.wav","vj_battledroid/step2.wav","vj_battledroid/step3.wav","vj_battledroid/step4.wav"}
ENT.SoundTbl_Idle = {"vj_battledroid/idle1.wav","vj_battledroid/idle2.wav","vj_battledroid/idle3.wav","vj_battledroid/idle4.wav"}
ENT.SoundTbl_CombatIdle = {"vj_battledroid/talk2.wav","vj_battledroid/talk8.wav","vj_battledroid/talk6.wav"}
ENT.SoundTbl_Alert = {"vj_battledroid/alert1.wav","vj_battledroid/alert2.wav","vj_battledroid/alert3.wav","vj_battledroid/alert4.wav"}
ENT.SoundTbl_OnReceiveOrder = {"vj_battledroid/roger.wav"}
ENT.SoundTbl_WeaponReload = {"vj_battledroid/talk4.wav","vj_battledroid/talk5.wav","vj_battledroid/talk7.wav"}
ENT.SoundTbl_OnGrenadeSight = {"vj_battledroid/grenade1.wav","vj_battledroid/grenade2.wav","vj_battledroid/grenade3.wav","vj_battledroid/grenade4.wav"}
ENT.SoundTbl_OnKilledEnemy = {"vj_battledroid/talk1.wav","vj_battledroid/talk3.wav"}
ENT.SoundTbl_Pain = {"vj_battledroid/pain1.wav","vj_battledroid/pain2.wav","vj_battledroid/pain3.wav","vj_battledroid/pain4.wav","vj_battledroid/pain5.wav","vj_battledroid/pain6.wav","vj_battledroid/pain7.wav","vj_battledroid/pain8.wav","vj_battledroid/pain9.wav",}
ENT.SoundTbl_Impact = {"ambient/energy/spark1.wav","ambient/energy/spark2.wav","ambient/energy/spark3.wav","ambient/energy/spark4.wav"}
ENT.SoundTbl_Death = {"vj_battledroid/death1.wav","vj_battledroid/death2.wav","vj_battledroid/death3.wav","vj_battledroid/death4.wav","vj_battledroid/death5.wav"}

	-- ====== Weapon Settings ====== --
ENT.Weapon_NoSpawnMenu = false 
ENT.WeaponSpread = .75
ENT.AllowWeaponReloading = false
ENT.MoveRandomlyWhenShooting = true

function ENT:CustomOnInitialize()
	self:Give("weapon_vj_b2_blaster")
	self.AnimTbl_Run = {self:GetSequenceActivity(self:LookupSequence("run_protected_all"))}
end

---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomOnTakeDamage_OnBleed(dmginfo,hitgroup)
	if (dmginfo:IsBulletDamage()) then
		local attacker = dmginfo:GetAttacker()
		if math.random(1,2) == 1 then
			if math.random(1,2) == 1 then dmginfo:ScaleDamage(0.50) else dmginfo:ScaleDamage(0.25) end
			self.DamageSpark1 = ents.Create("env_spark")
			self.DamageSpark1:SetKeyValue("Magnitude","1")
			self.DamageSpark1:SetKeyValue("Spark Trail Length","1")
			self.DamageSpark1:SetPos(dmginfo:GetDamagePosition())
			self.DamageSpark1:SetAngles(self:GetAngles())
			//self.DamageSpark1:Fire("LightColor", "255 255 255")
			self.DamageSpark1:SetParent(self)
			self.DamageSpark1:Spawn()
			self.DamageSpark1:Activate()
			self.DamageSpark1:Fire("StartSpark", "", 0)
			self.DamageSpark1:Fire("StopSpark", "", 0.001)
			self:DeleteOnRemove(self.DamageSpark1)
			if self.HasSounds == true && self.HasImpactSounds == true then VJ_EmitSound(self,"vj_impact_metal/bullet_metal/metalsolid"..math.random(1,10)..".wav",70) end
		end
	end
end
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/