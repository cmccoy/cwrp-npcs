AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )

function ENT:Initialize()

	self:SetModel( "models/aat/aat_hull1.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(12000)
	end
	
	self.turret = ents.Create("prop_dynamic")
	self.turret:SetModel("models/aat/aat_turret1.mdl")
	self.turret:SetPos( self:GetPos() + Vector(0,0, 150) )
	self.turret:SetAngles( self:GetAngles() )
	self.turret:Spawn()
		self.turret:PhysicsDestroy()
		self.turret:Activate()
		self.turret:SetCollisionBounds(Vector(-3,-3,-0), Vector(3,3,3))
		self.turret:PhysicsInit(SOLID_BBOX)
	self.turret:SetColor(self:GetColor())

	self.turretPhys = self.turret:GetPhysicsObject()
	if IsValid(phys) then
		self.turretPhys:Wake() 
		self.turretPhys:SetMass(120000)
		self.turretPhys:SetVelocity(self:GetUp() * 500)
	end

	self:DeleteOnRemove(self.turret)
	
	self.gun = ents.Create("prop_dynamic")
	self.gun:SetModel("models/aat/aat_gun1.mdl")
	self.gun:SetPos( self:GetPos() + Vector(0,0, 150) )
	self.gun:SetAngles( self:GetAngles() )
	self.gun:Spawn()
		self.gun:PhysicsDestroy()
		self.gun:Activate()
		self.gun:SetCollisionBounds(Vector(-3,-3,-0), Vector(3,3,3))
		self.gun:PhysicsInit(SOLID_BBOX)
	self.gun:SetColor(self:GetColor())

	self.gunPhys = self.gun:GetPhysicsObject()
	if IsValid(phys) then
		self.gunPhys:Wake() 
		self.gunPhys:SetMass(120000)
		self.turretPhys:SetVelocity(self:GetForward() * 500)
	end

	self:DeleteOnRemove(self.gun)
	
	timer.Simple(0.2,function()
		if self:IsValid() then
			self.turret:SetColor(self:GetColor())
			self.gun:SetColor(self:GetColor())
			if self.gunnerlastangles != nil then
				self.turret:SetAngles(self.gunnerlastangles)
				self.gun:SetAngles( self.turret:GetAngles() +Angle(2.5,0,0))
			end
		end
	end)

	
	self.PhysObj = self:GetPhysicsObject()
	
	if ( self.PhysObj:IsValid() ) then
		self.PhysObj:EnableDrag( true )
		self.PhysObj:Wake()
	end

end

