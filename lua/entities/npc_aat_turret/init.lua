AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2016 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------
*/
ENT.Model = "models/aat/aat_turret1.mdl" -- Leave empty if using more than one model 

ENT.StartHealth = 0
---------------------------------------------------------------------------------------------------------------------------------------------
ENT.VJ_NPC_Class = {"CLASS_STARWARS_CIS"} 
ENT.PlayerFriendly = false -- Makes the SNPC friendly to the player and HL2 Resistance
ENT.VJ_IsHugeMonster = false -- Is this a huge monster?

-- Tank Base
ENT.Tank_AngleDiffuseNumber = 0
ENT.Tank_UseNegativeAngleDiffuseNumber = false -- true
ENT.Tank_AngleDiffuseGeneralNumber = 2.0
ENT.Tank_UsesRightAngles = false --true

ENT.ShellSpawnPos = Vector(-150,0,5.5)
ENT.ShellLightPos = Vector(-147,0,4.5)
ENT.ShellMuzzlePos = Vector(-287,0,4.5)

ENT.MG1SpawnPos = Vector(-69,14,83)
ENT.MG1MuzzlePos = Vector(-63,14,81)
ENT.MG1SpawnPos_muzzle = Vector(-63,14,81)

ENT.CrewLookPos = Vector(6000,0,100)

--ENT.ShellSpawnPos = Vector(0,-70,15)
--ENT.ShellLightPos = Vector(-75,0,0)
--ENT.ShellMuzzlePos = Vector(0,-75,12)
--ENT.ShellParticlePos1 = Vector(0,-75,18)
--ENT.ShellParticlePos2 = Vector(0,-75,18)
ENT.Tank_SeeFar = 5100 -- If the enemy is higher than this number, than don't shoot!
ENT.Tank_SeeLimit = 7000 -- How far can it see?
ENT.ShellShootUp = 150 --10
ENT.Tank_NotFacingTargetShootPos = 300
ENT.Tank_SeeClose = 230 -- If the enemy is closer than this number, than don't shoot!

ENT.Tank_MG_Damage = 9
ENT.Tank_MG_Cone = 7
ENT.MGShootUp = 30
ENT.MG1_Sound = "weapons/german/mg34_shoot.wav"

util.AddNetworkString("vj_mili_tiger_shooteffects")

---------------------------------------------------------------------------------------------------------------------------------------------







function ENT:CustomOnSchedule()
	if self.Dead == true then return end

	if self:GetEnemy() == nil then
		if self.Tank_ResetedEnemy == false then
		self.Tank_ResetedEnemy = true
		self:ResetEnemy() end
		//self:FindEnemySphere()
	else
		self.Tank_ResetedEnemy = false
		EnemyPos = self:GetEnemy():GetPos()
		EnemyPosToSelf = self:GetPos():Distance(EnemyPos)
		if EnemyPosToSelf > self.Tank_SeeLimit then -- If more than this, Don't fire!
			self.Tank_Status = 1
		elseif EnemyPosToSelf < self.Tank_SeeFar && EnemyPosToSelf > self.Tank_SeeClose then -- Between this two numbers than fire!
			self.Tank_Status = 0
		else
			self.Tank_Status = 1
		end
	end
end


function ENT:RangeAttack_Base()
if self.Tank_ProperHeightShoot == false then return end

local function Timer_ShellAttack()
	self:RangeAttack_Shell()
	self.FiringShell = false
end

if self.Tank_ShellReady == false && self.Tank_ShellRecharging == 0 then	
	if self.HasSounds == true then
	if GetConVarNumber("vj_npc_sd_rangeattack") == 0 then
	self.shootsd1 = CreateSound(self, "vehicles/tank_readyfire1.wav") self.shootsd1:SetSoundLevel(90)
	self.shootsd1:PlayEx(1,100) end end
	self.Tank_ShellReady = true
end

if self.Dead == false then
	timer.Create( "timer_shell_attack"..self.Entity:EntIndex(), 0.3, 1, Timer_ShellAttack ) 
	
	end
	
	
	--self:RangeAttack_MG1()
	
	
end


function ENT:CustomInitialize()

	self:SetNoDraw(true)
	self.Tank_ShellRecharging = 0
	self.Tank_ShellReloading = 0
	self.Tank_ShellFiring = 0
	self.Tank_StartFiring = 0
	self.Tank_Shellcycle = 0
	
	self.TankGunLevel = 0
	self.TankGunismoving = 0 

	self.Tank_MGRecharging = 0
	self.Tank_MGbulletfired = 0
	
	self.Whee2 = ents.Create("prop_dynamic")
    self.Whee2:SetModel("models/aat/aat_turret1.mdl")
	self.Whee2:SetPos(self:GetPos())
	self.Whee2:SetParent( self )
	self.Whee2:SetAngles( self:GetAngles() + Angle(0, 180, 0) )
	self.Whee2:Spawn()
	self:DeleteOnRemove(self.Whee2)
	self.Whee2:SetNoDraw(false)
	self.Whee2.Owner = self
	
   
	self.Wheela = ents.Create("prop_dynamic")
	self.Wheela:SetModel("models/aat/aat_gun1.mdl")
	self.Wheela:SetPos( self:GetPos() + Vector(50, 0, 40) )
	self.Wheela:SetParent( self )
	self.Wheela:SetSkin( self:GetSkin() )
	self.Wheela:SetAngles( self:GetAngles() + Angle(0, 180, 0) )
	self.Wheela:Spawn()
   
	-- self.Wheelaa = ents.Create("prop_dynamic")
	-- self.Wheelaa:SetModel("models/aat/aat_arms1.mdl")
	-- self.Wheelaa:SetPos( Vector(10, -2, 50) )
	-- self.Wheelaa:SetParent( self )
	-- self.Wheelaa:SetSkin( self:GetSkin() )
	-- self.Wheelaa:SetAngles( self:GetAngles() )
	-- self.Wheelaa:Spawn()
end




function ENT:CustomOnThink_AIEnabled()
	if self.Dead == true then return end

	if self:GetParent():GetEnemy() != nil && self:GetEnemy() == nil then
	self:SetEnemy(self:GetParent():GetEnemy())
	end
	
	if self.Tank_GunnerIsTurning == true then
	
	VJ_STOPSOUND(self.tank_movingsd) 
	
	else

	VJ_STOPSOUND(self.tank_movingsd) 
	
	end
	
	self:CustomOnSchedule()
	
	if self.Tank_FacingTarget == false then self.FiringShell = false end
	if self.Tank_ShellReady == false then self.FiringShell = false end
	if self.Tank_Status == 0 then
		if self:GetEnemy() == nil then
		self.Tank_Status = 1
		self.Tank_GunnerIsTurning = false
	else
	-- x = Forward | y = Right | z = Up
	
	--local fucktraces = { start = self:GetPos(), endpos = self:GetPos() + self:GetUp()*-85, filter = self }
	--local tr = util.TraceEntity( fucktraces, self ) 
	
	local fucktraces1 = { start = self:GetParent():GetPos(), endpos = self:GetParent():GetPos() + self:GetParent():GetUp()*-15, filter = self:GetParent() }
	local tr1 = util.TraceEntity( fucktraces1, self:GetParent() )
	
	local phys2 = self:GetParent():GetPhysicsObject()
	local Angle_Enemy = (self:GetEnemy():GetPos() - self:GetPos() /*+ Vector(80,0,80)*/):Angle()
	local Angle_Current = self:GetAngles()
	if self.Tank_UseNegativeAngleDiffuseNumber == true then
	Angle_Diffuse = self:AngleDiffuse(Angle_Enemy.y,Angle_Current.y-self.Tank_AngleDiffuseNumber) else -- Cannon looking direction
	Angle_Diffuse = self:AngleDiffuse(Angle_Enemy.y,Angle_Current.y+self.Tank_AngleDiffuseNumber) end -- Cannon looking direction
	local Heigh_Ratio = (self:GetEnemy():GetPos().z - self:GetPos().z ) / self:GetPos():Distance(Vector(self:GetEnemy():GetPos().x,self:GetEnemy():GetPos().y,self:GetPos().z))
	-- ^^^ How high it can shoot ^^^ --
	
	
	if math.abs(Heigh_Ratio) < 0.35 && (self:GetEnemy():GetPos().z >= self:GetPos().z) && math.abs(Heigh_Ratio) >= 0.15 then 
	
	if self.TankGunLevel == 1 then
	
	self.Tank_ProperHeightShoot = true 
	
	else
	
	if self.TankGunLevel == 0 then
		
	if self.TankGunismoving != 1 then
	self.TankGunismoving = 1
	timer.Simple(0.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.6, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.8, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.9, function() if self.Dead == false then 
	self.TankGunismoving = 0 
	self.TankGunLevel = 1
	end end)
	
	end
	end
	
	if self.TankGunLevel == -1 then
		
	if self.TankGunismoving != 1 then
	self.TankGunismoving = 1
	timer.Simple(0.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.6, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	
	timer.Simple(0.8, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(1.0, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(1.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(1.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(1.5, function() if self.Dead == false then 
	self.TankGunismoving = 0 
	self.TankGunLevel = 1
	end end)
	
	end
	end
	
	
	
	--self:SetLocalAngles( self:GetLocalAngles() + Angle(0,3.0,0))
	
	self.Tank_ProperHeightShoot = false 
	
	end
	--else 
	
	--self.Tank_ProperHeightShoot = false 
	
	end
	
	if math.abs(Heigh_Ratio) < 0.24 && (self:GetEnemy():GetPos().z < self:GetPos().z) && math.abs(Heigh_Ratio) >= 0.15 then 
	
	if self.TankGunLevel == -1 then
	
	self.Tank_ProperHeightShoot = true 
	
	else
	
	if self.TankGunLevel == 0 then
		
	if self.TankGunismoving != 1 then
	self.TankGunismoving = 1
	timer.Simple(0.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.6, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.7, function() if self.Dead == false then 
	self.TankGunismoving = 0 
	self.TankGunLevel = -1
	end end)
	
	end
	end
	
	if self.TankGunLevel == 1 then
		
	if self.TankGunismoving != 1 then
	self.TankGunismoving = 1
	timer.Simple(0.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.6, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.8, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	
	timer.Simple(1.0, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(1.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(1.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(1.5, function() if self.Dead == false then 
	self.TankGunismoving = 0 
	self.TankGunLevel = -1
	end end)
	
	end
	end
	--self:SetLocalAngles( self:GetLocalAngles() + Angle(0,3.0,0))
	
	self.Tank_ProperHeightShoot = false 
	
	end
	--else 
	
	--self.Tank_ProperHeightShoot = false 
	
	end
	
	
	if math.abs(Heigh_Ratio) < 0.15 then 
	
	if self.TankGunLevel == 0 then
	
	self.Tank_ProperHeightShoot = true 
	
	else
	
	if self.TankGunLevel == 1 then
		
	if self.TankGunismoving != 1 then
	self.TankGunismoving = 1
	timer.Simple(0.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.6, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.8, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(-1,0,0)) end end)
	timer.Simple(0.9, function() if self.Dead == false then 
	self.TankGunismoving = 0 
	self.TankGunLevel = 0 
	end end)
	
	end
	end
	
	if self.TankGunLevel == -1 then
		
	if self.TankGunismoving != 1 then
	self.TankGunismoving = 1
	timer.Simple(0.2, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.4, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.6, function() if self.Dead == false then self.Wheela:SetLocalAngles( self.Wheela:GetLocalAngles() + Angle(1,0,0)) end end)
	timer.Simple(0.7, function() if self.Dead == false then 
	self.TankGunismoving = 0 
	self.TankGunLevel = 0
	end end)
	
	end
	end
	
	--self:SetLocalAngles( self:GetLocalAngles() + Angle(0,3.0,0))
	
	self.Tank_ProperHeightShoot = false 
	
	end
	
	else 
	
	if math.abs(Heigh_Ratio) >= 0.35 then 
	self.Tank_ProperHeightShoot = false 
	end
	
	end
	
	
	
	
	
	
	self.Tank_GunnerIsTurning = false
	
	
	
	
	
	
	
	if math.abs(Angle_Diffuse) < self.Tank_AngleDiffuseGeneralNumber && self.FiringShell == false && math.abs(Heigh_Ratio) < 0.35 && self:GetPos():Distance(self:GetEnemy():GetPos()) > self.Tank_SeeClose then
	
	if (math.abs(Heigh_Ratio) < 0.35 && self.TankGunLevel == 1 && math.abs(Heigh_Ratio) >= 0.15 && (self:GetEnemy():GetPos().z >= self:GetPos().z)) || (math.abs(Heigh_Ratio) < 0.24 && self.TankGunLevel == -1 && math.abs(Heigh_Ratio) >= 0.15 && (self:GetEnemy():GetPos().z < self:GetPos().z)) || (math.abs(Heigh_Ratio) < 0.15 && self.TankGunLevel == 0) then
	-- If the diffuse and the height and the enemy distance is higher than the self.Tank_SeeClose than shoot!
		self.Tank_GunnerIsTurning = false
		self.FiringShell = true
		self.Tank_FacingTarget = true
		if self:Visible(self:GetEnemy()) then
		if GetConVarNumber("vj_npc_norange") == 0 then
		self:RangeAttack_Base() end end
	end
		
	elseif Angle_Diffuse > self.Tank_AngleDiffuseGeneralNumber then
		//self:SetLocalAngles( self:GetLocalAngles() + Angle(0,2,0))
		
		if Angle_Diffuse > (self.Tank_AngleDiffuseGeneralNumber + 10) then
		if (self:GetParent():IsValid()) && (self:GetParent().Tank_Status != nil) then
		if self:GetParent().Tank_Status == 1 then
		
		if phys2:IsValid() then
		--if phys2:GetVelocity():Length() <= 10 then 
		
		local tracedata11 = {}
		tracedata11.start = self:GetParent():GetPos() + Vector(0,0,45)
		tracedata11.endpos = self:GetParent():GetPos() + Vector(0,0,-65)
		tracedata11.filter = {self,self:GetParent()}
		local tr11 = util.TraceLine(tracedata11)
		
		
		
		if ( tr11.HitWorld ) && self:GetParent().driving == 0 then
		
		
		self:GetParent():SetLocalAngles( self:GetParent():GetLocalAngles() + Angle(0,1.8,0))
		phys2:SetAngles(self:GetParent():GetAngles())
		
		end
		--end
		end
		
		end
		end
		end
		
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,3.1,0))
		self.Tank_GunnerIsTurning = true
		self.Tank_FacingTarget = false
		self.FiringShell = false

	elseif Angle_Diffuse < -self.Tank_AngleDiffuseGeneralNumber then
		//self:SetLocalAngles( self:GetLocalAngles() + Angle(0,-2,0))
		
		
		if Angle_Diffuse < -1*(self.Tank_AngleDiffuseGeneralNumber + 10) then
		if (self:GetParent():IsValid()) && (self:GetParent().Tank_Status != nil) then
		if self:GetParent().Tank_Status == 1 then
		
		if phys2:IsValid() then
		--if phys2:GetVelocity():Length() <= 10 then 

		local tracedata12 = {}
		tracedata12.start = self:GetParent():GetPos() + Vector(0,0,45)
		tracedata12.endpos = self:GetParent():GetPos() + Vector(0,0,-65)
		tracedata12.filter = {self,self:GetParent()}
		local tr12 = util.TraceLine(tracedata12)
		
		if ( tr12.HitWorld ) && self:GetParent().driving == 0 then

		
		self:GetParent():SetLocalAngles( self:GetParent():GetLocalAngles() + Angle(0,-1.8,0))
		phys2:SetAngles(self:GetParent():GetAngles())
		
		--end
		end
		end
		
		end 
		end
		
		end
		
		
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,-3.1,0))
		self.Tank_GunnerIsTurning = true
		self.Tank_FacingTarget = false
		self.FiringShell = false
		
	
	end
  end
 end
end

function ENT:RangeAttack_MG1()
	if self.Dead == true then return end
	if self.Tank_MGRecharging == 1 then return end
	
	if self.Tank_FacingTarget == true && IsValid(self:GetEnemy()) && self:GetEnemy() != NULL && self:GetEnemy() != nil then
	if self:Visible(self:GetEnemy()) || self.Tank_MGRecharging != 1 then

	local bulletshootpos = (self:GetEnemy():GetPos()-self:LocalToWorld(self.MG1SpawnPos) + self:GetUp()*-self.MGShootUp + self:GetRight()*-23)
	local bulletsrc = self.Whee:GetPos() --self:LocalToWorld(self.MG1SpawnPos)
	
	local bulletmuzzlepos = self:LocalToWorld(self.MG1MuzzlePos)
	
	if (math.Rand(1,4) >= 2 ) then 
	
	bulletshootpos = (self:GetEnemy():GetPos()-self:LocalToWorld(self.MG1SpawnPos) + self:GetUp()*-(math.random(-35,25)) + self:GetRight()*-(math.random(-45,50)))
	
	end
	
	if (SERVER) then
		sound.Play(Sound(self.MG1_Sound),self:GetPos(),80,math.random(90,100))
	end

	
	
	local flash = ents.Create("env_muzzleflash")
	flash:SetPos(self:LocalToWorld(self.MG1MuzzlePos))
	flash:SetKeyValue("scale","0.8")

	if self.Tank_UsesRightAngles == true then
	flash:SetKeyValue("angles",tostring(self:GetRight():Angle())) else
	flash:SetKeyValue("angles",tostring(self:GetForward():Angle())) end
	flash:Fire( "SetParent", self, 0 )
	flash:Fire( "Fire", 0, 0 )
	


	
	/*
	local Emitter = ParticleEmitter(self:LocalToWorld(self.MG1MuzzlePos))
	if IsValid(Emitter) then 
	if GetConVarNumber("vj_wep_nomuszzleflash") == 0 && IsValid(self) then
	
	local EffectCode = Emitter:Add("effects/muzzleflash"..math.random(1,4),bulletmuzzlepos)
		EffectCode:SetVelocity(bulletshootpos + 1.1 * self:GetVelocity())
		//EffectCode:SetAirResistance(200)
		EffectCode:SetDieTime(math.Rand(0.05,0.05)) -- How much time until it dies
		EffectCode:SetStartAlpha(math.Rand(200,255)) -- Transparency
		EffectCode:SetStartSize(math.Rand(5,6)) -- Size of the effect
		EffectCode:SetEndSize(math.Rand(16,20)) -- Size of the effect at the end (The effect slowly trasnsforms to this size)
		EffectCode:SetRoll(math.Rand(480,540))
		EffectCode:SetRollDelta(math.Rand(-1,1)) -- How fast it rolls
		EffectCode:SetColor(255,255,255) -- The color of the effect
		//EffectCode:SetGravity(Vector(0,0,100)) -- The Gravity
		
	local EffectCode2 = Emitter:Add("effects/yellowflare",bulletmuzzlepos)
		EffectCode2:SetVelocity(bulletshootpos + 1.1 * self:GetVelocity())
		EffectCode2:SetAirResistance(160)
		EffectCode2:SetDieTime(0.05)
		EffectCode2:SetStartAlpha(255)
		EffectCode2:SetEndAlpha(0)
		EffectCode2:SetStartSize(.5)
		EffectCode2:SetEndSize(15)
		EffectCode2:SetRoll(math.Rand(180, 480))
		EffectCode2:SetRollDelta(math.Rand(-1, 1))
		EffectCode2:SetColor(255, 255, 255)
	end
	Emitter:Finish()
	end
	*/
	

	
	
	
	local bullet = {}
		bullet.Attacker = self.Whee --self
		bullet.Num = 1
		bullet.Src = bulletsrc
		bullet.Dir = bulletshootpos
		bullet.Tracer = 1
		bullet.TracerName = "Tracer"
		bullet.Force = 5
		bullet.Damage = self.Tank_MG_Damage
		bullet.AmmoType = "SMG1"
		bullet.Spread = Vector((self.Tank_MG_Cone /60)/4,(self.Tank_MG_Cone /60)/4,0)
		bullet.Callback = function(attacker, trace, dmginfo)
			dmginfo:SetDamageType(DMG_BULLET)
		end
		self.Whee:FireBullets(bullet)
		
		self.Tank_MGbulletfired = self.Tank_MGbulletfired + 1
		
		if self.Tank_MGbulletfired >= 50 then
		
		self.Tank_MGRecharging = 1
		self.Tank_MGbulletfired = 0
		timer.Simple(6.0,function() if self.Dead == false then self.Tank_MGRecharging = 0 end end)

		end
		
		end
		end
end



function ENT:RangeAttack_Shell()
	if self.Dead == true then return end
	if self.Tank_ShellRecharging == 1 then return end
	if self.Dead == false then if GetConVarNumber("ai_disabled") == 0 then
	if self.Tank_ProperHeightShoot == false then return end
	if /*self.Tank_FacingTarget == true &&*/ IsValid(self:GetEnemy()) && self:GetEnemy() != NULL && self:GetEnemy() != nil then
	if self:Visible(self:GetEnemy()) then
	if self.HasSounds == true then
	if GetConVarNumber("vj_npc_sd_rangeattack") == 0 then

	sound.Play("impact/aat_main_cannon_fire.mp3", self:GetPos(), 180, 100, 1)
	end end
	//self:StartShootEffects()
	self.FireLight1 = ents.Create("light_dynamic")
	self.FireLight1:SetKeyValue("brightness", "4")
	self.FireLight1:SetKeyValue("distance", "400")
	self.FireLight1:SetPos(self.Wheela:LocalToWorld(self.ShellLightPos))
	self.FireLight1:SetLocalAngles(self:GetAngles())
	self.FireLight1:Fire("Color", "255 150 60")
	self.FireLight1:SetParent(self)
	self.FireLight1:Spawn()
	self.FireLight1:Activate()
	self.FireLight1:Fire("TurnOn", "", 0)
	self:DeleteOnRemove(self.FireLight1)
	timer.Simple(0.1,function() if self.Dead == false then self.FireLight1:Remove() end end)
	-- local panis1= "smoke_exhaust_01"
	-- local panis2 = "Advisor_Pod_Steam_Continuous"
	-- ParticleEffect(panis2,self.Wheela:LocalToWorld(self.ShellMuzzlePos),Angle(0,0,90),self.Wheela)
	-- ParticleEffect(panis1,self.Wheela:LocalToWorld(self.ShellMuzzlePos),Angle(0,0,90),self.Wheela)

	-- timer.Simple(1, function() self:StopParticles() end)

	util.ScreenShake( self:GetPos(), 100, 200, 1, 2500 )
	local flash = ents.Create("env_muzzleflash")
		flash:SetPos(self.Wheela:LocalToWorld(self.ShellMuzzlePos))
		flash:SetKeyValue("scale","4")
		if self.Tank_UsesRightAngles == true then
		flash:SetKeyValue("angles",tostring(self:GetRight():Angle())) else
		flash:SetKeyValue("angles",tostring((self.Wheela:GetForward()*1):Angle())) end
		flash:Fire( "Fire", 0, 0 )
	local dust = EffectData()
		dust:SetOrigin(self:GetParent():GetPos())
		dust:SetScale(500)
		util.Effect( "ThumperDust", dust )
	if self.Tank_FacingTarget == true then
	ShootPos = (self:GetEnemy():GetPos()-self:GetPos() + self:GetUp()*-self.ShellShootUp) end
	if self.Tank_FacingTarget == false then
	if self.Tank_UsesRightAngles == true then
	ShootPos = (self:GetRight()*self.Tank_NotFacingTargetShootPos) else
	
	if self.TankGunLevel == 1 then
	ShootPos = (self:GetForward()*self.Tank_NotFacingTargetShootPos) + self:GetUp()*30
	else
	
	if self.TankGunLevel == - 1 then
	ShootPos = (self:GetForward()*self.Tank_NotFacingTargetShootPos) + self:GetUp()*-20 
	else
	ShootPos = (self:GetForward()*self.Tank_NotFacingTargetShootPos)
	
	end 
	end
	
	end end
	if self:GetEnemy():GetClass() == "npc_vj_mili_tiger_red" or self:GetEnemy():GetClass() == "npc_vj_mili_tiger_redg" or self:GetEnemy():GetClass() == "npc_vj_milifri_tiger_red" or self:GetEnemy():GetClass() == "npc_vj_milifri_tiger_redg" then
	ShootPos = (self:GetEnemy():GetPos()-self:GetPos() + self:GetUp()*-self.ShellShootUp) end
	
	
	local physe = self:GetEnemy():GetPhysicsObject()
	
	
	if physe != nil then
	if physe:IsValid() then
	if physe:GetVelocity():Length() > 30 then
	if self.Tank_FacingTarget == true then
	if self:GetPos():Distance(self:GetEnemy():GetPos()) > 250 then
	
	local physe_enemypos = self:GetEnemy():GetPos()
	
	if physe:GetVelocity():Length() > 600 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity():GetNormalized()*100
	else
	
	if self:GetPos():Distance(self:GetEnemy():GetPos()) > 250 && self:GetPos():Distance(self:GetEnemy():GetPos()) < 1450 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(2,4))*0.1)
	end
	
	if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 1450 && self:GetPos():Distance(self:GetEnemy():GetPos()) < 2150 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(5,7))*0.1)
	end
	
	if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 2150 && self:GetPos():Distance(self:GetEnemy():GetPos()) < 4500 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(6,7))*0.1)
	end
	
	if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 4500 && self:GetPos():Distance(self:GetEnemy():GetPos()) < 6000 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(7,8))*0.1)
	end
	
	if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 6000 && self:GetPos():Distance(self:GetEnemy():GetPos()) < 9900 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(7,9))*0.1)
	end
	
	if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 9900 then
	physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(8,9))*0.1)
	end
	
		if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 300 && (math.random(1,12) < 3) then
		
			physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(0,1))*0.1)
			
		end
		
		if self:GetPos():Distance(self:GetEnemy():GetPos()) >= 3300 && (math.random(1,6) >= 3) then
		
			physe_enemypos = self:GetEnemy():GetPos() + physe:GetVelocity()*((math.random(9,11))*0.1)
			
		end
	
	end
	
	if self.TankGunLevel == 1 then ShootPos = (physe_enemypos - self:GetPos() + self:GetUp()*-(self.ShellShootUp - 25)) end
	if self.TankGunLevel == -1 then ShootPos = (physe_enemypos - self:GetPos() + self:GetUp()*-(self.ShellShootUp + 5)) end
	if self.TankGunLevel == 0 then ShootPos = (physe_enemypos - self:GetPos() + self:GetUp()*-(self.ShellShootUp - 0)) end
	
	end
	end
	end
	end
	end
	
	
	
	local Projectile_Shell = ents.Create("obj_vj_tri_droid_rocket")
	Projectile_Shell:SetPos(self.Wheela:LocalToWorld(self.ShellSpawnPos))
	Projectile_Shell:Activate()
	Projectile_Shell:Spawn()
	Projectile_Shell:PointAtEntity(self:GetEnemy())
			local phys = Projectile_Shell:GetPhysicsObject()
			if phys:IsValid() then
				phys:SetVelocity(Projectile_Shell:GetForward()*4000)
			end
	
	
	-- local Projectile_Shell = ents.Create("obj_vj_wrist_rocket")
	-- 	Projectile_Shell:SetPos(self.Wheela:LocalToWorld(self.ShellSpawnPos))
	-- 	Projectile_Shell:SetAngles(ShootPos:Angle())
	-- 	Projectile_Shell:Spawn()
	-- 	Projectile_Shell:Activate()
	-- 	Projectile_Shell:SetOwner(self)
	-- local phys = Projectile_Shell:GetPhysicsObject()
	-- 	if phys:IsValid() then
	-- 	phys:ApplyForceCenter((ShootPos * 760000))
	-- end
		self.Tank_ShellReady = false
		self.FiringShell = false
		--Projectile_Shell:SetModelScale(1,0)
		self.Tank_ShellRecharging = 1
		timer.Simple((1.55 + (math.random(0,10))*0.1), function() if self.Dead == false then self.Tank_ShellRecharging = 0 end end)
		else
		self.Tank_ShellReady = false
		self.FiringShell = false
		self.Tank_FacingTarget = false
	end
   end
  end
 end
end









/*-----------------------------------------------
	*** Copyright (c) 2012-2016 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/