AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2015 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = "models/aat/aat1.mdl"

ENT.StartHealth = 5000




---------------------------------------------------------------------------------------------------------------------------------------------
ENT.VJ_NPC_Class = {"CLASS_STARWARS_CIS"} 
ENT.PlayerFriendly = false -- Makes the SNPC friendly to the player and HL2 Resistance
ENT.DeathEntityType = "aat_hull" 

ENT.AlertSoundLevel = 60
ENT.IdleSoundLevel = 60
ENT.EnemyTalkSoundLevel = 60
ENT.BreathSoundLevel = 85
ENT.DeathSoundLevel = 100

ENT.IdleSoundPitch1 = 90
ENT.IdleSoundPitch2 = 90
ENT.AlertSoundPitch1 = 80
ENT.AlertSoundPitch2 = 80
ENT.EnemyTalkSoundPitch1 = 80
ENT.EnemyTalkSoundPitch2 = 80

-- Tank Base
ENT.Tank_GunnerENT = "npc_aat_turret"
ENT.Tank_SpawningAngle = 180
ENT.Tank_CollisionBoundSize = 300
ENT.Tank_CollisionBoundUp = 0
ENT.Tank_AngleDiffuseNumber = 180 --270
ENT.Tank_ForwardSpead = 350 -- Forward speed
ENT.Tank_MoveAwaySpeed = 350 -- Move away speed
ENT.Tank_UseGetRightForSpeed = false -- Should it use GetRight instead of GetForward when driving?
ENT.Tank_SeeFar = 2500 -- If the enemy is higher than this number, than don't shoot!
ENT.Tank_SeeLimit = 8000 -- How far can it see?
ENT.Tank_SeeClose = 450 -- If the enemy is closer than this number, than move!
ENT.ImmuneDamagesTable = {DMG_BULLET,DMG_BUCKSHOT,DMG_PHYSGUN, DMG_CLUB}
ENT.Tank_TurningSpeed = 2.2

ENT.WaitBeforeDeathTime = 1.5 -- Time until the SNPC spawns its corpse and gets removedz

ENT.SoundTbl_Breath = ("sdkfz222/idle.wav")

ENT.SoundTbl_TankDie = {}


/*
ENT.SoundTbl_TankCombatIdle = {"germancommandtank/attack.wav","germancommandtank/attackalt.wav","germancommandtank/defend.wav","germancommandtank/defendalt.wav","germancommandtank/attackalt2.wav"}
ENT.SoundTbl_TankCombatAlert = {"germancommandtank/unit.wav"}
ENT.SoundTbl_TankCombatAlert_inf = {"germancommandtank/infantry.wav"}
ENT.SoundTbl_TankCombatAlert_veh = {"germancommandtank/armor.wav","germancommandtank/atsupport.wav"}
ENT.SoundTbl_TankCombatReinforcement = {"germancommandtank/reinforcements.wav","germancommandtank/rogerthat.wav"}
*/

util.AddNetworkString("vj_mili_tiger_spawneffects")
util.AddNetworkString("vj_mili_tiger_moveeffects")
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:Tank_GunnerSpawnPosition()
	return self:GetPos() + self:GetUp()*120 +self:GetForward()*130 --+self:GetUp()*-1 --+self:GetRight()*-3.5 +self:GetForward()*-5
end

function ENT:Tank_GunnerSpawnPosition2()
	return self:GetPos() +self:GetUp()*0 +self:GetRight()*0 +self:GetForward()*0
end

function ENT:CustomOnInitialize()
	self:CustomInitialize_CustomTank()
	self:PhysicsInit( SOLID_BBOX )
	self:SetAngles(self:GetAngles()+Angle(0,self.Tank_SpawningAngle,0))
	
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(12000)
	end
	self.HasDeathRagdoll = true -- If set to false, it will not spawn the regular ragdoll of the SNPC
	self.DeathCorpseEntityClass = self.DeathEntityType -- The entity class it creates | "UseDefaultBehavior" = Let the base automatically detect the type



	self:SetNoDraw(true)
		
	
	self.TankEnemy = NULL
	self.turret = ents.Create("npc_aat_turret")
	self.turret:Spawn()
	self.turret:SetPos(self:Tank_GunnerSpawnPosition())
	self.turret:SetAngles(self:GetAngles())
	self.turret:SetParent( self )
	
	
    self.hull = ents.Create("prop_dynamic")
	self.hull:SetModel("models/aat/aat_hull1.mdl")
	self.hull:SetPos( self:LocalToWorld( Vector( 0, 0, 20 ) ) )
	self.hull:SetParent( self )
	self.hull:SetAngles( self:GetAngles() )
	self.hull:Spawn()

	self.boxhull = ents.Create("prop_dynamic")
    self.boxhull:SetModel("models/hunter/blocks/cube025x025x025.mdl")
    self.boxhull:SetRenderMode(RENDERMODE_TRANSALPHA)
    self.boxhull:SetColor(255,0,0,100)
    self.boxhull:SetPos(self:LocalToWorld( Vector( 200, 0, 0 ) ))
	self.boxhull:Spawn()
		self.boxhull:PhysicsDestroy()
		self.boxhull:Activate()
		self.boxhull:SetCollisionBounds(Vector(-3,-3,-0), Vector(3,3,3))
		self.boxhull:PhysicsInit(SOLID_BBOX)
	local phys = self.boxhull:GetPhysicsObject()
	if IsValid(phys) then 
		phys:Wake() 
		phys:SetMass(10000)
	end

	self.boxhull:SetNoDraw(true)

	constraint.Weld(self.boxhull, self, 0, 0, 0, true, true)

	self.radiotalking = 0
	self.combatidletalkingtimer = 0
	self.combatreinforcetimer = 0
	self.KT_NextSoldierSpawnT = 0
	self.KT_GoingToSpawnThem = false
	self.KT_Spawnedsoldier = 0
	self.rancombatreinforce = 0
	
	self.gunnerlastangles = 0
	
	self.driving = 0
	self.nostopdriving = 0
	self.nostopdrivingwhenfrontblocked = 0
	self.IsBlockedOnTheFront = 0
	self.targetpath1 = 0	--Vector to move to
	
end
	
function ENT:StartSpawnEffects()
	//net.Start("vj_mili_tiger_spawneffects")
	//net.WriteEntity(self)
	//net.Broadcast()
end
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:StartMoveEffects()
	//net.Start("vj_mili_tiger_moveeffects")
	//net.WriteEntity(self)
	//net.Broadcast()
end  

function ENT:TANK_MOVINGSOUND()
	if self.HasSounds == true && GetConVarNumber("vj_npc_sd_footstep") == 0 then
	self.tank_movingsd = CreateSound(self,"heracles421/galactica_vehicles/aat_engine.wav") self.tank_movingsd:SetSoundLevel(70)
	self.tank_movingsd:PlayEx(1,100)
	end
end


function ENT:CustomOnSchedule()
	if self:Health() <= 0 or self.Dead == true then return end

	self:IdleSoundCode()

	if self:GetEnemy() == nil then
		if self.Tank_ResetedEnemy == false then
		self.Tank_ResetedEnemy = true
		self:ResetEnemy() end
	else
		EnemyPos = self:GetEnemy():GetPos()
		EnemyPosToSelf = self:GetPos():Distance(EnemyPos)
		if EnemyPosToSelf > self.Tank_SeeLimit then -- If larger than this number than, move
			self.Tank_Status = 0
		elseif EnemyPosToSelf < self.Tank_SeeFar && EnemyPosToSelf > self.Tank_SeeClose then -- If between this two numbers, stay still
			self.Tank_Status = 1
		else
			self.Tank_Status = 0
		end
	end
end

function ENT:CustomOnThink()

	local phys_tank_fix = self.Entity:GetPhysicsObject()
	if phys_tank_fix:IsValid() && phys_tank_fix:GetVelocity():Length() < 5 then
		phys_tank_fix:AddVelocity(self:GetUp():GetNormal()*9)
	end

	if GetConVarNumber("vj_npc_noidleparticle") == 1 then return end
	timer.Simple(0.1,function()
		if IsValid(self) && self.Dead == false then
			self:StartSpawnEffects()
		end
	end)
	
	if self:Health() < (self.StartHealth*0.30) then
		if Tank_NextLowHealthSmokeT && CurTime() > self.Tank_NextLowHealthSmokeT then
			//ParticleEffectAttach("vj_rpg2_smoke2", PATTACH_ABSORIGIN_FOLLOW, self, 0)
			self.Spark1 = ents.Create("env_spark")
			self.Spark1:SetKeyValue("MaxDelay",0.01)
			self.Spark1:SetKeyValue("Magnitude","8")
			self.Spark1:SetKeyValue("Spark Trail Length","3")
			self:GetNearDeathSparkPositions()
			self.Spark1:SetAngles(self:GetAngles())
			//self.Spark1:Fire("LightColor", "255 255 255")
			self.Spark1:SetParent(self)
			self.Spark1:Spawn()
			self.Spark1:Activate()
			self.Spark1:Fire("StartSpark", "", 0)
			self.Spark1:Fire("kill", "", 0.1)
			self:DeleteOnRemove(self.Spark1)

			self.Tank_NextLowHealthSmokeT = CurTime() + math.random(4,6)
		else
			self.Tank_NextLowHealthSmokeT = CurTime() + math.random(4,6)
		end
	end
end

function ENT:CustomOnThink_AIEnabled()
	if self.Dead == true then return end
	//timer.Simple(0.1, function() if self.Dead == false then ParticleEffect("smoke_exhaust_01",self:LocalToWorld(Vector(150,30,30)),Angle(0,0,0),self) end end)
	//timer.Simple(0.2, function() if self.Dead == false then self:StopParticles() end end)
	for k,v in pairs(ents.FindInSphere(self:GetPos(),135)) do
		self:TANK_RUNOVER_DAMAGECODE(v)
	end


	local tracedataAD = {}
	tracedataAD.start = self:GetPos() + Vector(0,0,40)
	tracedataAD.endpos = self:GetPos() + Vector(0,0,-65)
	tracedataAD.filter = {self,self.Gunner}
	local trAD = util.TraceLine(tracedataAD)
	
	local fucktraces = { start = self:GetPos(), endpos = self:GetPos() + self:GetUp()*-15, filter = self }
	local tr = util.TraceEntity( fucktraces, self ) 
	if ( trAD.HitWorld ) then
	local phys = self.Entity:GetPhysicsObject()
	if phys:IsValid() then
	if phys:GetVelocity():Length() > 10 || self.driving == 1 then  /*print("This fucking tank is moving")*/
	self.Tank_IsMoving = true
	self:TANK_MOVINGSOUND()
	self:StartMoveEffects()
	else VJ_STOPSOUND(self.tank_movingsd) self.Tank_IsMoving = false /*print(self:GetClass().." Is not fucking Moving!")*/ end end end
	if ( !trAD.HitWorld ) then
	VJ_STOPSOUND(self.tank_movingsd) self.Tank_IsMoving = false /*print(self:GetClass().." Is not fucking Moving!")*/ end
	
	self:CustomOnSchedule()

	self.IsBlockedOnTheFront = 0
	
	local phys_tank = self.Entity:GetPhysicsObject()
	
	if self.nostopdrivingwhenfrontblocked == 0 then
	
	if phys_tank:IsValid() then
	if phys_tank:GetVelocity():Length() > 5 then
	
	local tracedataADf = {}
	tracedataADf.start = self:GetPos() + Vector(0,0,50)
	tracedataADf.endpos = self:GetPos() + phys_tank:GetVelocity():GetNormalized()*600
	tracedataADf.filter = {self,self.Gunner,self.Gunner2,self.Gunner3}
	local trADf = util.TraceLine(tracedataADf)
	
	if ( trADf.Hit ) then
	if trADf.Entity != nil && trADf.Entity:IsValid() then
	if (trADf.Entity:IsNPC() && trADf.Entity:Health() > 0) then
	if !trADf.Entity:IsPlayer() && trADf.Entity.IsVJBaseSNPC == true && trADf.Entity.Dead != true then
	if VJ_PICKRANDOMTABLE(trADf.Entity.VJ_NPC_Class) == VJ_PICKRANDOMTABLE(self.VJ_NPC_Class) then
		self.IsBlockedOnTheFront = 1
	end
	end
	end
	end
	end
	
	end
	end
	
	end
	
	if ( trAD.HitWorld ) then
	--if self.IsBlockedOnTheFront != 1 then
	if phys_tank:IsValid() then
	if self.targetpath1 != nil then
	if self.targetpath1 != 0 then
	if ( self:GetEnemy() == nil || (self.nostopdriving == 1) || (self.nostopdriving == 0 && self.Gunner:GetEnemy() != nil && !(self.Gunner:Visible(self.Gunner:GetEnemy())) ) ) && self.IsBlockedOnTheFront != 1 && self:GetPos():Distance(self.targetpath1) >= 150 then
	
	local Angle_TankParallel = Angle(0,self:GetAngles().y,0)
	local AD_targetpath1pos = Vector(self.targetpath1.x,self.targetpath1.y,0)
	local AD_tankpos = Vector(self:GetPos().x,self:GetPos().y,0)
	self.driving = 1
	local Angle_TargetPath = (AD_targetpath1pos - AD_tankpos ):Angle()	--(self.targetpath1 - self:GetPos() ):Angle()
	local Angle_MyCurrentAngle = Angle_TankParallel --self:GetAngles()

	Angle_Diffuse_TargetPath = self:AngleDiffuse(Angle_TargetPath.y,Angle_MyCurrentAngle.y+self.Tank_AngleDiffuseNumber) -- Tank looking direction

	if Angle_Diffuse_TargetPath >= -15 && Angle_Diffuse_TargetPath <= 15 then
	
		if self.Tank_UseGetRightForSpeed == true then
		phys_tank:SetVelocity(self:GetRight():GetNormal()*self.Tank_MoveAwaySpeed*1) else
		phys_tank:SetVelocity(-self:GetForward():GetNormal()*self.Tank_MoveAwaySpeed*1) end
	
	end
	
	
	if Angle_Diffuse_TargetPath > 3 then
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,self.Tank_TurningSpeed,0))
		phys_tank:SetAngles(self:GetAngles())
	else
	if Angle_Diffuse_TargetPath < -3 then
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,-self.Tank_TurningSpeed,0))
		phys_tank:SetAngles(self:GetAngles())
	end
	end
	
	
	else
		self.driving = 0
	end
	end
	end
	end
	

	
	end

	if self.targetpath1 == nil || self.targetpath1 == 0 then
	if self.driving == 1 then
			self.driving = 0
	end
	end

	
	
	if self.Tank_Status == 0 then
		if self:GetEnemy() == nil then
		self.Tank_Status = 1
	else
		if self.driving == 0 then
	-- x = Forward | y = Right | z = Up | {x,y,z}
	-- To make it go opposite:
		-- Change the +15 to -15 and -15 to 15 
		-- Change the forwad spead(Tank_ForwardSpead) to their opposite quotation(+ to -)
		-- Change the turning speed(Tank_TurningSpeed) to their opposite quotation(+ to -)
	local phys = self.Entity:GetPhysicsObject()
	local Angle_Enemy = (self:GetEnemy():GetPos() -self:GetPos() +Vector(0,0,80)):Angle() 
	local Angle_Current = self:GetAngles() 
	local Angle_Diffuse = self:AngleDiffuse(Angle_Enemy.y,Angle_Current.y+self.Tank_AngleDiffuseNumber)
	local Heigh_Ratio = (self:GetEnemy():GetPos().z - self:GetPos().z ) / self:GetPos():Distance(Vector(self:GetEnemy():GetPos().x,self:GetEnemy():GetPos().y,self:GetPos().z))

	local fucktraces = { start = self:GetPos(), endpos = self:GetPos() + self:GetUp()*-105, filter = self }
	local tr = util.TraceEntity( fucktraces, self )
	
	local tracedata11 = {}
	tracedata11.start = self:GetPos() + Vector(0,0,20)
	tracedata11.endpos = self:GetPos() + Vector(0,0,-65)
	tracedata11.filter = {self}
	local tr11 = util.TraceLine(tracedata11)
	
	if ( tr11.HitWorld ) then
	if Heigh_Ratio < 0.15 then -- If it is that high than move away from it
		if phys:IsValid() then -- To help the gunner shoot
		if self.Tank_UseGetRightForSpeed == true then
		phys:SetVelocity(self:GetRight():GetNormal()*self.Tank_MoveAwaySpeed) else
		phys:SetVelocity(-self:GetForward():GetNormal()*self.Tank_MoveAwaySpeed) end
	if Angle_Diffuse > -15 then
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,self.Tank_TurningSpeed,0))
		phys:SetAngles(self:GetAngles())
	elseif Angle_Diffuse < 15 then
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,-self.Tank_TurningSpeed,0))
		phys:SetAngles(self:GetAngles())
		end
	end
	--//if self:GetEnemy().VJ_IsHugeMonster == false then
	elseif math.abs(Heigh_Ratio) > 1 && math.abs(Heigh_Ratio) < 0.6 then -- If it is that high than move toward it
		if phys:IsValid() then -- Run over
		if self.Tank_UseGetRightForSpeed == true then
		phys:SetVelocity(self:GetRight():GetNormal()*self.Tank_MoveAwaySpeed) else
		phys:SetVelocity(self:GetForward():GetNormal()*self.Tank_MoveAwaySpeed) end
	if Angle_Diffuse > 15 then
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,self.Tank_TurningSpeed,0))
		phys:SetAngles(self:GetAngles())
	elseif Angle_Diffuse < -15 then
		self:SetLocalAngles( self:GetLocalAngles() + Angle(0,-self.Tank_TurningSpeed,0))
		phys:SetAngles(self:GetAngles())
		end
	 --// end
	 end
	end
   end
   
   end
  end
 end
end



---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomOnPriorToKilled(dmginfo,hitgroup)	
	
	
	timer.Simple(0,function()
	if self:IsValid() then
	self:EmitSound("vj_mili_tank/tank_death2.wav",100,100)
	util.BlastDamage(self,self,self:GetPos(),200,40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
	ParticleEffect("vj_explosion2",self:GetPos(),Angle(0,0,0),nil)
	end
   end)
   
   	timer.Simple(0.2,function()
	if self:IsValid() then
	self:EmitSound("vj_mili_tank/tank_death2.wav",100,100)
	util.BlastDamage(self,self,self:GetPos(),200,40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
	ParticleEffect("vj_explosion2",self:GetPos(),Angle(0,0,0),nil)
	end
   end)
	
	timer.Simple(0.6,function()
	if self:IsValid() then
	self:EmitSound("vj_mili_tank/tank_death2.wav",100,100)
	util.BlastDamage(self,self,self:GetPos(),200,40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
	ParticleEffect("vj_explosion2",self:GetPos(),Angle(0,0,0),nil)
	end
   end)
	
	timer.Simple(1,function()
	if self:IsValid() then
	self:EmitSound("vj_mili_tank/tank_death2.wav",100,100)
	util.BlastDamage(self,self,self:GetPos(),200,40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
	ParticleEffect("vj_explosion2",self:GetPos(),Angle(0,0,0),nil)
	end
   end)
   
  	timer.Simple(1.3,function()
	if self:IsValid() then
	self:EmitSound("vj_mili_tank/tank_death2.wav",100,100)
	util.BlastDamage(self,self,self:GetPos(),200,40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
	ParticleEffect("vj_explosion2",self:GetPos() +self:GetForward()*-40,Angle(0,0,0),nil)
	end
	end)
	
	
	timer.Simple(1.5,function()
	if self:IsValid() then
	self:EmitSound("vj_mili_tank/tank_death2.wav",100,100)
	self:EmitSound("vj_mili_tank/tank_death3.wav",100,100)
	util.BlastDamage(self,self,self:GetPos(),200,40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
	ParticleEffect("vj_explosion2",self:GetPos(),Angle(0,0,0),nil)
	self:Ignite( math.Rand( 16, 20 ), 0 )
	self:SetColor(Color(40,40,40))
	end
   end)
end

---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:GetNearDeathSparkPositions()
	local randpos = math.random(1,7)
	if randpos == 1 then return self.Spark1:SetLocalPos(self:GetPos() +self:GetForward()*90 +self:GetUp()*60 +self:GetRight()*-40) else
	if randpos == 2 then return self.Spark1:SetLocalPos(self:GetPos() +self:GetForward()*50 +self:GetUp()*50 +self:GetRight()*40) end
	if randpos == 3 then return self.Spark1:SetLocalPos(self:GetPos()+self:GetUp()*90 +self:GetRight()*40 +self:GetForward()*20) end 
	if randpos == 4 then return self.Spark1:SetLocalPos(self:GetPos()+self:GetUp()*90 +self:GetRight()*60) end
	if randpos == 5 then return self.Spark1:SetLocalPos(self:GetPos() +self:GetForward()*30 +self:GetUp()*70 +self:GetRight()*70) end
	if randpos == 6 then return self.Spark1:SetLocalPos(self:GetPos() +self:GetForward()*80 +self:GetUp()*40 +self:GetRight()*30) end
	if randpos == 7 then return self.Spark1:SetLocalPos(self:GetPos() +self:GetForward()*60 +self:GetUp()*70 +self:GetRight()*50) end
	end
end

function ENT:CustomOnDeath_AfterCorpseSpawned(dmginfo,hitgroup,GetCorpse)
	util.BlastDamage( self, self, self:GetPos(),400, 40)
	util.ScreenShake(self:GetPos(), 100, 200, 1, 2500)
end

function ENT:TANK_RUNOVER_DAMAGECODE(argent)
// if self.HasMeleeAttack == false then return end
if argent == NULL or argent == nil then return end
if GetConVarNumber("vj_npc_nomelee") == 1 then return end

local function Tank_DoDamage()
	if GetConVarNumber("vj_npc_dif_normal") == 1 then argent:TakeDamage(6,self,self) end -- Normal
	if GetConVarNumber("vj_npc_dif_easy") == 1 then argent:TakeDamage(6 /2,self,self) end -- Easy
	if GetConVarNumber("vj_npc_dif_hard") == 1 then argent:TakeDamage(6 *1.5,self,self) end -- Hard
	if GetConVarNumber("vj_npc_dif_hellonearth") == 1 then argent:TakeDamage(6 *2.5,self,self) end  -- Hell On Earth
	VJ_DestroyCombineTurret(self,argent)
	argent:SetVelocity(self:GetForward()*-800)
end

	if (argent:IsNPC() && argent:Disposition(self) == 1 && argent:Health() > 0) then
	if !argent:IsPlayer() && argent.IsVJBaseSNPC == true && argent.VJ_IsHugeMonster == false then
		Tank_DoDamage()
		self:Tank_Sound_RunOver()
	end
	if (argent:IsNPC() && argent.IsVJBaseSNPC != true && !table.HasValue(self.TankTbl_DontRunOver,argent:GetClass())) or (argent:IsPlayer() && self.PlayerFriendly == false && GetConVarNumber("ai_ignoreplayers") == 0 && argent:Alive() && self.Tank_IsMoving == true) then
		Tank_DoDamage()
		self:Tank_Sound_RunOver()
	end
 end
end