ENT.Base 			= "npc_vj_human_base"
ENT.Type 			= "ai"
ENT.PrintName 		= "Dwarf Spider Droid"
ENT.Author 			= "Vac"
ENT.Contact 		= ""
ENT.Purpose 		= "Spawn it and fight with it!"
ENT.Instructions 	= "Click on the spawnicon to spawn it."
ENT.Category		= "Star Wars"

game.AddParticles( "particles/striderbuster.pcf" )
PrecacheParticleSystem( "striderbuster_break_c" )
PrecacheParticleSystem( "striderbuster_explode_smoke" )

if (CLIENT) then
	local Name = "Dwarf Spider Droid"
	local LangName = "npc_vj_swbd_dwarf_spider"
	language.Add(LangName, Name)
	killicon.Add(LangName,"HUD/killicons/default",Color(255,80,0,255))
	language.Add("#"..LangName, Name)
	killicon.Add("#"..LangName,"HUD/killicons/default",Color(255,80,0,255))
end