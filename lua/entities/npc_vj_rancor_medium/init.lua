AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = "models/props/ig/rancor.mdl" 
ENT.StartHealth = 7500 
ENT.MovementType = VJ_MOVETYPE_GROUND
ENT.HullType = HULL_LARGE
ENT.VJ_IsHugeMonster = true
---------------------------------------------------------------------------------------------------------------------------------------------

ENT.Bleeds = true -- Does the SNPC bleed? (Blood decal, particle and etc.)
ENT.BloodParticle = "blood_impact_red_01" -- Particle that the SNPC spawns when it's damaged
ENT.BloodDecal = "Blood" -- (Red = Blood) (Yellow Blood = YellowBlood) | Leave blank for none
ENT.BloodDecalRate = 1000 -- The more the number is the more chance is has to spawn | 1000 is a good number for yellow blood, for red blood 500 is good | Make the 
ENT.HasMeleeAttack = true -- Should the SNPC have a melee attack?

ENT.HasDeathRagdoll = false -- If set to false, it will not spawn the regular ragdoll of the SNPC
ENT.Immune_Dissolve = true -- Immune to Dissolving | Example: Combine Ball
ENT.Immune_Physics = true 
ENT.HasDeathAnimation = true -- Does it play an animation when it dies?
ENT.AnimTbl_Death = {"Roshan_die"} -- Death Animations

	-- ====== Flinching Code ====== --
ENT.Flinches = 0 -- 0 = No Flinch | 1 = Flinches at any damage | 2 = Flinches only from certain damages
ENT.FlinchingChance = 14 -- chance of it flinching from 1 to x | 1 will make it always flinch
ENT.FlinchingSchedules = {SCHED_FLINCH_PHYSICS} -- If self.FlinchUseACT is false the it uses this | Common: SCHED_BIG_FLINCH, SCHED_SMALL_FLINCH, SCHED_FLINCH_PHYSICS
	-- ====== Sound File Paths ====== --
-- Leave blank if you don't want any sounds to play
ENT.SoundTbl_FootStep = {"roshan/Roshan_footstep1.mp3","roshan/Roshan_footstep2.mp3"}
ENT.SoundTbl_Idle = {""}
ENT.SoundTbl_Alert = {""}
ENT.SoundTbl_BeforeMeleeAttack = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
ENT.SoundTbl_MeleeAttack = {"roshan/attack1.mp3","roshan/attack2.mp3","roshan/attack3.mp3"}
ENT.SoundTbl_MeleeAttackMiss = {""}
ENT.SoundTbl_Pain = {""}
ENT.SoundTbl_Death = {"roshan/death.wav"}



ENT.MeleeAttackSoundLevel = 510
ENT.BreathSoundLevel = 510
ENT.IdleSoundLevel = 510
ENT.CombatIdleSoundLevel = 510
ENT.OnReceiveOrderSoundLevel = 510
ENT.FollowPlayerSoundLevel = 510
ENT.UnFollowPlayerSoundLevel = 510
ENT.BeforeHealSoundLevel = 510
ENT.AfterHealSoundLevel = 510
ENT.OnPlayerSightSoundLevel = 510
ENT.AlertSoundLevel = 510
ENT.CallForHelpSoundLevel = 510
ENT.BecomeEnemyToPlayerSoundLevel = 510
ENT.BeforeMeleeAttackSoundLevel = 510
ENT.ExtraMeleeAttackSoundLevel = 510
ENT.MeleeAttackMissSoundLevel = 510
ENT.MeleeAttackSlowPlayerSoundLevel = 510
ENT.BeforeRangeAttackSoundLevel = 510
ENT.RangeAttackSoundLevel = 510
ENT.BeforeLeapAttackSoundLevel = 510
ENT.LeapAttackJumpSoundLevel = 510
ENT.LeapAttackDamageSoundLevel = 510
ENT.LeapAttackDamageMissSoundLevel = 510
ENT.OnKilledEnemySoundLevel = 510
ENT.PainSoundLevel = 510
ENT.ImpactSoundLevel = 510
ENT.DamageByPlayerSoundLevel = 510
ENT.DeathSoundLevel = 510

---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomInitialize()
	self:SetCollisionBounds(Vector(139/2, 138/2, 320/2), Vector(-133/2, -134/2, -5/2))
	self:SetModelScale(.5, 0)
	self.AnimTbl_Walk = {VJ_SequenceToActivity(self,"Walk")}
	self.AnimTbl_Run = {VJ_SequenceToActivity(self,"Walk")}
end




function ENT:MultipleMeleeAttacks()
	local EnemyDistance = self:VJ_GetNearestPointToEntityDistance(self:GetEnemy(),self:GetPos():Distance(self:GetEnemy():GetPos()))
	if EnemyDistance > 0 && EnemyDistance < 95 then
		local randattack = math.random(1,5)
		self.MeleeAttackDistance = 70
		if randattack == 1 then
			self.TimeUntilMeleeAttackDamage = 0.6
			self.MeleeAttackAngleRadius = 100 -- What is the attack angle radius? | 100 = In front of the SNPC | 180 = All around the SNPC
			self.MeleeAttackDamageAngleRadius = 100
			self.MeleeAttackAnimationDelay = 0 -- It will wait certain amount of time before playing the animation
			self.MeleeAttackDistance = 70 -- How close does it have to be until it attacks?
			self.MeleeAttackDamageDistance = 280 -- How far the damage goes
			self.AnimTbl_MeleeAttack = {"Attack1"}
			self.MeleeAttackDamage = 500
			self.MeleeAttackDamageType = DMG_SLASH -- Type of Damage
			self.MeleeAttackExtraTimers = {}
			self.MeleeAttackReps = 1.5
			self.NextMeleeAttackTime = 0.4
			self.NextAnyAttackTime_Melee = 0.4
			self.SoundTbl_BeforeMeleeAttack = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
			self.SoundTbl_MeleeAttack = {"roshan/attack1.mp3","roshan/attack2.mp3","roshan/attack3.mp3"}
			self.SoundTbl_MeleeAttackMiss = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}

		elseif randattack == 2 then
			self.TimeUntilMeleeAttackDamage = 0.6
			self.MeleeAttackAnimationDelay = 0 -- It will wait certain amount of time before playing the animation
			self.MeleeAttackDistance = 110 -- How close does it have to be until it attacks?
			self.MeleeAttackDamageDistance = 280 -- How far the damage goes
			self.AnimTbl_MeleeAttack = {"Attack2"}
			self.MeleeAttackDamage = 500
			self.MeleeAttackDamageType = DMG_SLASH -- Type of Damage
			self.MeleeAttackExtraTimers = {}
			self.MeleeAttackReps = 1.5
			self.NextMeleeAttackTime = 0.4
			self.NextAnyAttackTime_Melee = 0.4
			self.SoundTbl_BeforeMeleeAttack = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
			self.SoundTbl_MeleeAttack = {"roshan/attack1.mp3","roshan/attack2.mp3","roshan/attack3.mp3"}
			self.SoundTbl_MeleeAttackMiss = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
		elseif randattack == 3 then

			self.TimeUntilMeleeAttackDamage = 0.75
			self.MeleeAttackAnimationDelay = 0 -- It will wait certain amount of time before playing the animation
			self.MeleeAttackDistance = 90 -- How close does it have to be until it attacks?
			self.MeleeAttackDamageDistance = 340 -- How far the damage goes
			self.AnimTbl_MeleeAttack = {"Attack3"}
			self.MeleeAttackDamage = 500
			self.MeleeAttackDamageType = DMG_SLASH -- Type of Damage
			self.MeleeAttackExtraTimers = {}
			self.MeleeAttackReps = 1.65
			self.NextMeleeAttackTime = 0.45
			self.NextAnyAttackTime_Melee = 0.45
			self.SoundTbl_BeforeMeleeAttack = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
			self.SoundTbl_MeleeAttack = {"roshan/slam.mp3"}
			self.SoundTbl_MeleeAttackMiss = {}

		elseif randattack == 4 then
			self.TimeUntilMeleeAttackDamage = 0.6
			self.MeleeAttackAnimationDelay = 0 -- It will wait certain amount of time before playing the animation
			self.MeleeAttackDistance = 110 -- How close does it have to be until it attacks?
			self.MeleeAttackDamageDistance = 280 -- How far the damage goes
			self.AnimTbl_MeleeAttack = {"Attack1"}
			self.MeleeAttackDamage = 500
			self.MeleeAttackDamageType = DMG_SLASH -- Type of Damage
			self.MeleeAttackExtraTimers = {}
			self.MeleeAttackReps = 1.5
			self.NextMeleeAttackTime = 0.4
			self.NextAnyAttackTime_Melee = 0.4
			self.SoundTbl_BeforeMeleeAttack = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
			self.SoundTbl_MeleeAttack = {"roshan/attack1.mp3","roshan/attack2.mp3","roshan/attack3.mp3"}
			self.SoundTbl_MeleeAttackMiss = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
		elseif randattack == 5 then
			self.TimeUntilMeleeAttackDamage = 0.6
			self.MeleeAttackAnimationDelay = 0 -- It will wait certain amount of time before playing the animation
			self.MeleeAttackDistance = 110 -- How close does it have to be until it attacks?
			self.MeleeAttackDamageDistance = 280 -- How far the damage goes
			self.AnimTbl_MeleeAttack = {"Attack2"}
			self.MeleeAttackDamage = 500
			self.MeleeAttackDamageType = DMG_SLASH -- Type of Damage
			self.MeleeAttackExtraTimers = {}
			self.MeleeAttackReps = 1.5
			self.NextMeleeAttackTime = 0.4
			self.NextAnyAttackTime_Melee = 0.4
			self.SoundTbl_BeforeMeleeAttack = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}
			self.SoundTbl_MeleeAttack = {"roshan/attack1.mp3","roshan/attack2.mp3","roshan/attack3.mp3"}
			self.SoundTbl_MeleeAttackMiss = {"roshan/before1.mp3","roshan/before.mp3","roshan/before.mp3"}

		end
	end
end