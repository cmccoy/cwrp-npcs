AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = {"models/vj_starwars/droid/commando.mdl"} -- The game will pick a random model from the table when the SNPC is spawned | Add as many as you want 
ENT.StartHealth = GetConVarNumber("vj_swbd_commando_h")
ENT.HullType = HULL_HUMAN
---------------------------------------------------------------------------------------------------------------------------------------------
ENT.VJ_NPC_Class = {"CLASS_STARWARS_CIS"} -- NPCs with the same class with be allied to each other
ENT.HasMeleeAttack = true -- Should the SNPC have a melee attack?
ENT.MeleeAttackDamage = GetConVarNumber("vj_swbd_commando_d")
ENT.FootStepTimeRun = 0.25 -- Next foot step sound when it is running
ENT.FootStepTimeWalk = 0.5 -- Next foot step sound when it is walking
ENT.HasGrenadeAttack = true -- Should the SNPC have a grenade attack?
ENT.DropWeaponOnDeath = false
ENT.IdleAlwaysWander = true
ENT.CanOpenDoors = true
ENT.DisableWandering = false
ENT.GibOnDeathDamagesTable = {"All"} -- Damages that it gibs from | "UseDefault" = Uses default damage types | "All" = Gib from any damage
	-- ====== Flinching Code ====== --
ENT.CanFlinch = 1 -- 0 = Don't flinch | 1 = Flinch at any damage | 2 = Flinch only from certain damages
ENT.AnimTbl_Flinch = {ACT_FLINCH_PHYSICS} -- If it uses normal based animation, use this
	-- ====== Sound File Paths ====== --
-- Leave blank if you don't want any sounds to play
ENT.SoundTbl_FootStep = {"vj_battledroid/step1.wav","vj_battledroid/step2.wav","vj_battledroid/step3.wav","vj_battledroid/step4.wav"}
ENT.SoundTbl_Idle = {"vj_battledroid/idle1.wav","vj_battledroid/idle2.wav","vj_battledroid/idle3.wav","vj_battledroid/idle4.wav"}
ENT.SoundTbl_CombatIdle = {"vj_battledroid/talk2.wav","vj_battledroid/talk8.wav","vj_battledroid/talk6.wav"}
ENT.SoundTbl_Alert = {"vj_battledroid/alert1.wav","vj_battledroid/alert2.wav","vj_battledroid/alert3.wav","vj_battledroid/alert4.wav"}
ENT.SoundTbl_OnReceiveOrder = {"vj_battledroid/roger.wav"}
ENT.SoundTbl_WeaponReload = {"vj_battledroid/talk4.wav","vj_battledroid/talk5.wav","vj_battledroid/talk7.wav"}
ENT.SoundTbl_OnGrenadeSight = {"vj_battledroid/grenade1.wav","vj_battledroid/grenade2.wav","vj_battledroid/grenade3.wav","vj_battledroid/grenade4.wav"}
ENT.SoundTbl_OnKilledEnemy = {"vj_battledroid/talk1.wav","vj_battledroid/talk3.wav"}
ENT.SoundTbl_Pain = {"vj_battledroid/pain1.wav","vj_battledroid/pain2.wav","vj_battledroid/pain3.wav","vj_battledroid/pain4.wav","vj_battledroid/pain5.wav","vj_battledroid/pain6.wav","vj_battledroid/pain7.wav","vj_battledroid/pain8.wav","vj_battledroid/pain9.wav",}
ENT.SoundTbl_Impact = {"ambient/energy/spark1.wav","ambient/energy/spark2.wav","ambient/energy/spark3.wav","ambient/energy/spark4.wav"}
ENT.SoundTbl_Death = {"vj_battledroid/death1.wav","vj_battledroid/death2.wav","vj_battledroid/death3.wav","vj_battledroid/death4.wav","vj_battledroid/death5.wav"}

function ENT:CustomOnInitialize()
	self:Give("weapon_vj_blaster")
end

-- Custom
ENT.SW_Droid_All = false
ENT.SW_Droid_Head = false
ENT.SW_Droid_RHand = false
ENT.SW_Droid_LHand = false
ENT.SW_Droid_RLeg = false
ENT.SW_Droid_LLeg = false
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomOnTakeDamage_OnBleed(dmginfo,hitgroup)
	if (dmginfo:IsBulletDamage()) then
		local attacker = dmginfo:GetAttacker()
		if math.random(1,2) == 1 then
			if math.random(1,2) == 1 then dmginfo:ScaleDamage(0.50) else dmginfo:ScaleDamage(0.25) end
			self.DamageSpark1 = ents.Create("env_spark")
			self.DamageSpark1:SetKeyValue("Magnitude","1")
			self.DamageSpark1:SetKeyValue("Spark Trail Length","1")
			self.DamageSpark1:SetPos(dmginfo:GetDamagePosition())
			self.DamageSpark1:SetAngles(self:GetAngles())
			//self.DamageSpark1:Fire("LightColor", "255 255 255")
			self.DamageSpark1:SetParent(self)
			self.DamageSpark1:Spawn()
			self.DamageSpark1:Activate()
			self.DamageSpark1:Fire("StartSpark", "", 0)
			self.DamageSpark1:Fire("StopSpark", "", 0.001)
			self:DeleteOnRemove(self.DamageSpark1)
			if self.HasSounds == true && self.HasImpactSounds == true then VJ_EmitSound(self,"vj_impact_metal/bullet_metal/metalsolid"..math.random(1,10)..".wav",70) end
		end
	end
end
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:SetUpGibesOnDeath(dmginfo,hitgroup)
	local dogib = false
	self.DeathCorpseModel = {"models/hfg/starwars/droids/commandodroid/torso_bg.mdl"}
	if table.HasValue(self.DefaultGibDamageTypes,dmginfo:GetDamageType()) then
		dogib = true
		self.SW_Droid_All = true
		self.DeathCorpseModel = {"models/hfg/starwars/droids/commandodroid/torso.mdl"}
	end
	
	if self.HasGibDeathParticles == true then
		--
	end
	// explosion_turret_fizzle
	// explosion_turret_break
	if hitgroup == HITGROUP_HEAD or self.SW_Droid_All == true then
		dogib = true
		self.SW_Droid_Head = true
		self:CreateGibEntity("prop_ragdoll","models/hfg/starwars/droids/commandodroid/head.mdl",{Pos=self:GetPos(),Ang=self:GetAngles()+Angle(0,0,0),Vel="UseDamageForce",NoFade=true,RemoveOnCorpseDelete=true},function(gib) gib:GetPhysicsObject():SetMass(80) end)
	end
	if hitgroup == HITGROUP_RIGHTARM or self.SW_Droid_All == true then
		dogib = true
		self.SW_Droid_RHand = true
		self:CreateGibEntity("prop_ragdoll","models/hfg/starwars/droids/commandodroid/right_arm.mdl",{Pos=self:GetPos(),Ang=self:GetAngles()+Angle(0,0,0),Vel="UseDamageForce",NoFade=true,RemoveOnCorpseDelete=true},function(gib) gib:GetPhysicsObject():SetMass(80) end)
	end
	if hitgroup == HITGROUP_LEFTARM or self.SW_Droid_All == true then
		dogib = true
		self.SW_Droid_LHand = true
		self:CreateGibEntity("prop_ragdoll","models/hfg/starwars/droids/commandodroid/left_arm.mdl",{Pos=self:GetPos(),Ang=self:GetAngles()+Angle(0,0,0),Vel="UseDamageForce",NoFade=true,RemoveOnCorpseDelete=true},function(gib) gib:GetPhysicsObject():SetMass(80) end)
	end
	if hitgroup == HITGROUP_RIGHTLEG or self.SW_Droid_All == true then
		dogib = true
		self.SW_Droid_RLeg = true
		self:CreateGibEntity("prop_ragdoll","models/hfg/starwars/droids/commandodroid/right_leg.mdl",{Pos=self:GetPos(),Ang=self:GetAngles()+Angle(0,0,0),Vel="UseDamageForce",NoFade=true,RemoveOnCorpseDelete=true},function(gib) gib:GetPhysicsObject():SetMass(80) end)
	end
	if hitgroup == HITGROUP_LEFTLEG or self.SW_Droid_All == true then
		dogib = true
		self.SW_Droid_LLeg = true
		self:CreateGibEntity("prop_ragdoll","models/hfg/starwars/droids/commandodroid/left_leg.mdl",{Pos=self:GetPos(),Ang=self:GetAngles()+Angle(0,0,0),Vel="UseDamageForce",NoFade=true,RemoveOnCorpseDelete=true},function(gib) gib:GetPhysicsObject():SetMass(80) end)
	end
	return dogib,{AllowCorpse=true}
end
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomGibOnDeathSounds(dmginfo,hitgroup)
	VJ_EmitSound(self,"ambient/energy/weld2.wav",70,math.random(80,100))
	return false
end
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomOnDeath_AfterCorpseSpawned(dmginfo,hitgroup,GetCorpse)
	if self.SW_Droid_All == true then
		//ParticleEffect("explosion_turret_break",self:GetPos()+self:OBBCenter(),Angle(0,0,0),nil)
		ParticleEffectAttach("explosion_turret_fizzle",PATTACH_POINT_FOLLOW,GetCorpse,0)
		timer.Simple(math.Rand(3,5), function() if IsValid(GetCorpse) then GetCorpse:StopParticles() end end)
	elseif self.SW_Droid_All == false then
		if self.SW_Droid_Head == true then
			GetCorpse:SetBodygroup(0,1)
		end
		if self.SW_Droid_RHand == true then
			GetCorpse:SetBodygroup(2,1)
		end
		if self.SW_Droid_LHand == true then
			GetCorpse:SetBodygroup(4,1)
		end
		if self.SW_Droid_RLeg == true then
			GetCorpse:SetBodygroup(3,1)
		end
		if self.SW_Droid_LLeg == true then
			GetCorpse:SetBodygroup(5,1)
		end
	end
end
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/