AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = {"models/props/impact/tri_droid.mdl"} -- The game will pick a random model from the table when the SNPC is spawned | Add as many as you want 
ENT.StartHealth = 12500
ENT.VJ_NPC_Class = {"CLASS_STARWARS_CIS"} 
ENT.MovementType = VJ_MOVETYPE_GROUND
ENT.HullType = HULL_MEDIUM_TALL
ENT.VJ_IsHugeMonster = true
---------------------------------------------------------------------------------------------------------------------------------------------
ENT.HasRangeAttack = true -- Should the SNPC have a range attack?
ENT.HasMeleeAttack = false -- Should the SNPC have a melee attack?
--ENT.RangeAttackEntityToSpawn = "obj_guard_shot" -- The entity that's released by the SNPC
ENT.RangeDistance = 2500 -- This is how far away it can shoot
ENT.RangeUseAttachmentForPos = false -- Should the projectile spawn on a attachment?
-- Side_Gun_1 | Side_Gun_2 | Front_Gun
-- ENT.RangeUseAttachmentForPosID = "Side_Gun_2"
ENT.UntilNextAttack_Range = 2 -- How much time until it can use a attack again? | Counted in Seconds
ENT.AllowIgnition = false -- Can this SNPC be set on fire?
ENT.HasDeathRagdoll = false
ENT.HasDeathAnimation = true -- Does it play an animation when it dies?
ENT.ImunneCombineBall = true
ENT.KnockBackV = true -- If true, it will cause a knockback to its enemy
ENT.KnockBackVForward = 700 -- Knockback Forward number
ENT.KnockBackVUP = 50 -- Knockback Up Number
ENT.IdleAlwaysWander = true
ENT.CanOpenDoors = true
ENT.DisableWandering = false
	-- ====== Sound File Paths ====== --
-- Leave blank if you don't want any sounds to play
ENT.SoundTbl_FootStep = {"combine_guard/step1.wav","combine_guard/step2.wav","combine_guard/step3.wav"}
ENT.SoundTbl_Idle = {}
ENT.SoundTbl_Alert = {}
ENT.SoundTbl_MeleeAttack = {""}
ENT.SoundTbl_Pain = {""}
ENT.SoundTbl_Death = {"combine_guard/die1.wav","combine_guard/die2.wav","combine_guard/die3.wav"}
ENT.RangeGetUp = 150 -- This makes the Object get up and it helps the Object reach farther
ENT.RangeGetForward = 1000 -- the width of the object spawning. Try to see what I mean.

ENT.RangeAttackAnimationStopMovement = true 
ENT.RangeAttackSoundLevel = 150

function ENT:CustomOnInitialize()
	self:SetCollisionBounds(Vector(139, 138, 720), Vector(-133, -134, -5))

	self.AnimTbl_Death = {VJ_SequenceToActivity(self,"gmod_death")}
	self.AnimTbl_RangeAttack = {VJ_SequenceToActivity(self,"attack_2")}
	self.AnimTbl_Walk = {VJ_SequenceToActivity(self,"Walk")}
	self.AnimTbl_Run = {VJ_SequenceToActivity(self,"Walk")}
end

---------------------------------------------------------------------------------------------------------------------------------------------
local NextThink = CurTime() + 5
function ENT:CustomOnThink() 
	if(CurTime() > NextThink) then
		if (self:GetEnemy()) then
			print(self:GetPos():DistToSqr(self:GetEnemy():GetPos()))
			print(300*300)
			print(self:GetPos():DistToSqr(self:GetEnemy():GetPos()) < 600*600)
			if (self:GetPos():DistToSqr(self:GetEnemy():GetPos()) < (600*600)) then
				for i = 1, 6 do
					if i == 1 then
						local vaporizer = ents.Create("obj_vj_tri_droid_vaporizer")
						vaporizer:SetPos(self:GetPos() + Vector(0,0,300))
						vaporizer:Activate()
						vaporizer:Spawn()
						vaporizer:PointAtEntity(self:GetEnemy())
						local phys = vaporizer:GetPhysicsObject()
						if phys:IsValid() then
							phys:SetVelocity(vaporizer:GetForward()*500)
						end
					else
						local vaporizer = ents.Create("obj_vj_tri_droid_vaporizer")
						vaporizer:SetPos(self:GetPos() + Vector(0,0,300))
						vaporizer:SetAngles(Angle(math.random(0,90),math.random(0,360), math.random(0,90)))
						vaporizer:Activate()
						vaporizer:Spawn()
						local phys = vaporizer:GetPhysicsObject()
						if phys:IsValid() then
							phys:AddVelocity(-vaporizer:GetUp() * 500)
						end
					end
				end
			end
		end
		NextThink = CurTime() + 5
	end
end

function ENT:CustomRangeAttackCode()
	local rand = math.random(1, 3)
	print(self:GetPos():Distance(self:GetEnemy():GetPos()))
	if (rand == 1) then
		local guntrace = {}
		guntrace.start = self:GetAttachment(2).Pos
		guntrace.endpos = self:GetEnemy():GetPos()
		guntrace.filter = self
		local tr = util.TraceLine(guntrace)
		local laserhitpos = tr.HitPos
		util.Decal("Scorch",laserhitpos + Vector(0,0,50),laserhitpos + Vector(0,0,-50))
		ParticleEffectAttach("advisor_pod_steam_01", PATTACH_POINT_FOLLOW, self, 2)
		util.ParticleTracerEx( "weapon_combine_ion_cannon_beam", self:GetPos(), laserhitpos, false, self:EntIndex(), 2)
		ParticleEffect("weapon_combine_ion_cannon_explosion", laserhitpos, Angle(-90,0,0), nil)
		util.ScreenShake(laserhitpos,5,5,2,500)
		sound.Play("ambient/explosions/explode_8.wav",laserhitpos,120,100)
		for k, v in pairs (ents.FindInSphere(laserhitpos,270)) do
			local damage = DamageInfo()
			local distance = laserhitpos:Distance(v:GetPos())
			local maxdist = 360
			if distance < maxdist then
				damage:SetAttacker(self)
				local dmg = (maxdist - distance) * 0.33
				damage:SetDamage(dmg)
				damage:SetDamageType(DMG_DISSOLVE)
				damage:SetAttacker(self)
				damage:SetDamagePosition(laserhitpos)
				v:TakeDamageInfo(damage,self)
			end
		end
	elseif(rand == 2) then
		timer.Simple(1, function()
			local rocket = ents.Create("obj_vj_tri_droid_rocket")
			rocket:SetPos(self:GetAttachment(1).Pos)
			rocket:Activate()
			rocket:Spawn()
			rocket:PointAtEntity(self:GetEnemy())
			local phys = rocket:GetPhysicsObject()
			if phys:IsValid() then
				phys:SetVelocity(rocket:GetForward()*4000)
			end
		end)
	else
		timer.Simple(2, function()
			local vaporizer = ents.Create("obj_vj_tri_droid_vaporizer")
			vaporizer:SetPos(self:GetAttachment(3).Pos)
			vaporizer:Activate()
			vaporizer:Spawn()
			vaporizer:PointAtEntity(self:GetEnemy())
			local phys = vaporizer:GetPhysicsObject()
			if phys:IsValid() then
				phys:SetVelocity(vaporizer:GetForward()*4000)
			end
			ParticleEffectAttach("advisor_pod_steam_01", PATTACH_POINT_FOLLOW, self, 3)
		end)
	end
end

function ENT:CustomOnTakeDamage_OnBleed(dmginfo,hitgroup)
	if (dmginfo:IsBulletDamage()) then
		local attacker = dmginfo:GetAttacker()
		if math.random(1,2) == 1 then
			if math.random(1,2) == 1 then dmginfo:ScaleDamage(0.50) else dmginfo:ScaleDamage(0.25) end
			self.DamageSpark1 = ents.Create("env_spark")
			self.DamageSpark1:SetKeyValue("Magnitude","1")
			self.DamageSpark1:SetKeyValue("Spark Trail Length","1")
			self.DamageSpark1:SetPos(dmginfo:GetDamagePosition())
			self.DamageSpark1:SetAngles(self:GetAngles())
			//self.DamageSpark1:Fire("LightColor", "255 255 255")
			self.DamageSpark1:SetParent(self)
			self.DamageSpark1:Spawn()
			self.DamageSpark1:Activate()
			self.DamageSpark1:Fire("StartSpark", "", 0)
			self.DamageSpark1:Fire("StopSpark", "", 0.001)
			self:DeleteOnRemove(self.DamageSpark1)
			if self.HasSounds == true && self.HasImpactSounds == true then VJ_EmitSound(self,"vj_impact_metal/bullet_metal/metalsolid"..math.random(1,10)..".wav",70) end
		end
	end
end

function ENT:CustomOnInitialKilled(dmginfo,hitgroup) 
	ParticleEffect( "portal_rift_01d", self:GetPos() + Vector(0,0,300), Angle( 0, 0, 0 ), nil )

	local timerIdentifier = self:GetCreationID().."_tridroid fade_"..CurTime()
	self:SetRenderMode( RENDERMODE_TRANSCOLOR ) --Allow transparency on model
	self.originalColor = self:GetColor()
	self.curFadeAlpha = self.originalColor.a
	timer.Create(timerIdentifier, .1, 26, function()
		if IsValid(self) then 
			self.curFadeAlpha = self.curFadeAlpha - 10
			self:SetColor( Color( self.originalColor.r, self.originalColor.g, self.originalColor.b, self.curFadeAlpha ) ) 
		else
			timer.Destroy(timerIdentifier)
		end
	end)
end

-- citadel_shockwave_e citadel_shockwave_g portal_rift_01d
function ENT:CustomOnRemove() 
	sound.Play("ambient/explosions/explode_8.wav",self:GetPos() + Vector(0,0,300),120,100)
	ParticleEffect( "portal_rift_flash_01", self:GetPos() + Vector(0,0,300), Angle( 0, 0, 0 ), nil )
	ParticleEffect( "citadel_shockwave_e", self:GetPos() + Vector(0,0,300), Angle( 0, 0, 0 ), nil )
end
