ENT.Base 			= "npc_vj_creature_base"
ENT.Type 			= "ai"
ENT.PrintName 		= "Tri Droid"
ENT.Author 			= "Vac"
ENT.Contact 		= ""
ENT.Purpose 		= "Spawn it and fight with it!"
ENT.Instructions 	= "Click on the spawnicon to spawn it."
ENT.Category		= "Star Wars"

game.AddParticles( "particles/skybox_smoke.pcf" )
PrecacheParticleSystem( "portal_rift_01d" )
PrecacheParticleSystem( "portal_rift_flash_01" )
PrecacheParticleSystem( "citadel_shockwave_e" )

if (CLIENT) then
	local Name = "Tri Droid"
	local LangName = "npc_vj_swbd_tri_droid"
	language.Add(LangName, Name)
	killicon.Add(LangName,"HUD/killicons/default",Color(255,80,0,255))
	language.Add("#"..LangName, Name)
	killicon.Add("#"..LangName,"HUD/killicons/default",Color(255,80,0,255))
end