AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2015 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = "models/combine_guard.mdl" -- Leave empty if using more than one model
ENT.StartHealth = 1500
ENT.MoveType = MOVETYPE_STEP
ENT.HullType = HULL_MEDIUM_TALL
---------------------------------------------------------------------------------------------------------------------------------------------
ENT.VJ_NPC_Class = {"CLASS_COMBINE"}
ENT.Bleeds = false -- Does the SNPC bleed? (Blood decal, particle and etc.)
-- ENT.BloodParticle = "blood_impact_yellow_01" -- Particle that the SNPC spawns when it's damaged
ENT.BloodDecal = "YellowBlood" -- (Red = Blood) (Yellow Blood = YellowBlood) | Leave blank for none
ENT.BloodDecalRate = 1000 -- The more the nuumber is the more chance is has to spawn | 1000 is a good number for yellow blood, for red blood 500 is good | Make the number smaller if you are using big decal like Antlion Splat, Which 5 or 10 is a really good number for this stuff
ENT.HasRangeAttack = true -- Should the SNPC have a range attack?
ENT.HasMeleeAttack = true -- Should the SNPC have a melee attack?
ENT.AnimTbl_MeleeAttack = {ACT_MELEE_ATTACK1} -- Melee Attack Animations
ENT.MeleeAttackAnimationDelay = 0 -- It will wait certain amount of time before playing the animation
ENT.MeleeAttackDistance = 45 -- How close does it have to be until it attacks?
ENT.MeleeAttackDamageDistance = 85 -- How far the damage goes
ENT.MeleeDistanceB = 45 -- Sometimes 45 is a good number but Sometimes needs a change
ENT.MeleeAttackHitTime = 0.6 -- This counted in seconds | This calculates the time until it hits something
ENT.UntilNextAttack_Melee = 0.5 -- How much time until it can use a attack again? | Counted in Seconds
ENT.MeleeAttackDamage = GetConVarNumber("vj_combine_guard_d")
ENT.MeleeAttackDamageType = DMG_SLASH -- Type of Damage
--ENT.RangeAttackEntityToSpawn = "obj_guard_shot" -- The entity that's released by the SNPC
ENT.RangeDistance = 500 -- This is how far away it can shoot
ENT.RangeToMeleeDistance = 0 -- How close does it have to be until it uses melee?
ENT.RangeUseAttachmentForPos = false -- Should the projectile spawn on a attachment?
ENT.UntilNextAttack_Range = 2 -- How much time until it can use a attack again? | Counted in Seconds
ENT.AfterShoot = 1.5 -- This is in seconds | How much time until the spit gets thrown | Example: After it does its animation, it waits 1 second until it actually shoot
ENT.AllowIgnition = false -- Can this SNPC be set on fire?
ENT.HasFootStepSound = true -- Should the SNPC make a footstep sound when it's moving?
ENT.CombineFriendly = true
ENT.HasDeathRagdoll = false
ENT.ImunneCombineBall = true
ENT.KnockBackV = true -- If true, it will cause a knockback to its enemy
ENT.KnockBackVForward = 700 -- Knockback Forward number
ENT.KnockBackVUP = 50 -- Knockback Up Number
ENT.HasFootStepSound = true -- Should the SNPC make a footstep sound when it's moving?
ENT.FootStepTimeRun = 0.3 -- Next foot step sound when it is running
ENT.FootStepTimeWalk = 0.6 -- Next foot step sound when it is walking
ENT.SquadName = "combine" -- Squad name, console error will happen if two groups that are enemy and try to squad!
ENT.IdleAlwaysWander = true
ENT.CanOpenDoors = true
ENT.DisableWandering = false
	-- ====== Sound File Paths ====== --
-- Leave blank if you don't want any sounds to play
ENT.SoundTbl_FootStep = {"combine_guard/step1.wav","combine_guard/step2.wav","combine_guard/step3.wav","combine_guard/step4.wav"}
ENT.SoundTbl_Idle = {"combine_guard/refind_enemy1.wav","combine_guard/refind_enemy2.wav","combine_guard/refind_enemy3.wav","combine_guard/refind_enemy4.wav"}
ENT.SoundTbl_Alert = {"combine_guard/go_alert1.wav","combine_guard/go_alert2.wav","combine_guard/go_alert3.wav"}
ENT.SoundTbl_MeleeAttack = {""}
ENT.SoundTbl_MeleeAttackMiss = {"combine_guard/stunstick_swing1.wav","combine_guard/stunstick_swing2.wav"}
ENT.SoundTbl_RangeAttack = {"combine_guard/cguard_fire.wav"}
ENT.SoundTbl_Pain = {""}
ENT.SoundTbl_Death = {"combine_guard/die1.wav","combine_guard/die2.wav","combine_guard/die3.wav"}
ENT.RangeGetUp = 150 -- This makes the Object get up and it helps the Object reach farther
ENT.RangeGetForward = 1000 -- the width of the object spawning. Try to see what I mean.

ENT.AnimTbl_RangeAttack = {"idle2angry"}
ENT.RangeAttackAnimationDecreaseLengthAmount = -0.5
ENT.RangeAttackAnimationStopMovement = true 
ENT.RangeAttackSoundLevel = 150
---------------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomInitialize()

end

function ENT:CustomRangeAttackCode() 
	local guntrace = {}
	guntrace.start = self:GetAttachment(1).Pos
	guntrace.endpos = self:GetEnemy():GetPos()
	guntrace.filter = self
	local tr = util.TraceLine(guntrace)
	local laserhitpos = tr.HitPos
	util.Decal("Scorch",laserhitpos + Vector(0,0,50),laserhitpos + Vector(0,0,-50))
	ParticleEffectAttach("advisor_pod_steam_01",PATTACH_POINT_FOLLOW,self,1)
	util.ParticleTracerEx( "weapon_combine_ion_cannon_beam", self:GetPos(), laserhitpos, false, self:EntIndex(), 1)
	ParticleEffect("weapon_combine_ion_cannon_explosion", laserhitpos, Angle(-90,0,0), nil)
	-- util.BlastDamage( self, self, laserhitpos, 270, 1 )
	util.ScreenShake(laserhitpos,5,5,2,500)
	--self:EmitSound("ambient/explosions/explode_7.wav")
	sound.Play("ambient/explosions/explode_8.wav",laserhitpos,120,100)
	-- PlayLoudSound(laserhitpos,"ambient/explosions/explode_8.wav",laserhitpos)
	for k, v in pairs (ents.FindInSphere(laserhitpos,270)) do
		local damage = DamageInfo()
		if (!string.match(v:GetClass(), "^npc_combine_.+")) then
			local distance = laserhitpos:Distance(v:GetPos())
			local maxdist = 360
			if distance < maxdist then
				damage:SetAttacker(self)
				local dmg = (maxdist - distance) * 0.33
				damage:SetDamage(dmg)
				damage:SetDamageType(DMG_DISSOLVE)
				damage:SetAttacker(self)
				damage:SetDamagePosition(laserhitpos)
				v:TakeDamageInfo(damage,self)
			end
		end
	end
end

/*-----------------------------------------------
	*** Copyright (c) 2012-2015 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/