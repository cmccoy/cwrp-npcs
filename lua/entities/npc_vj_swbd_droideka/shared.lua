ENT.Base 			= "npc_vj_human_base"
ENT.Type 			= "ai"
ENT.PrintName 		= "Droideka"
ENT.Author 			= "Vac"
ENT.Contact 		= ""
ENT.Purpose 		= "Spawn it and fight with it!"
ENT.Instructions 	= "Click on the spawnicon to spawn it."
ENT.Category		= "Star Wars"

game.AddParticles( "particles/striderbuster.pcf" )
PrecacheParticleSystem( "striderbuster_break" )

local shieldMat = Material("models/props_combine/portalball001_sheet")

if (CLIENT) then
	local Name = "Droideka"
	local LangName = "npc_vj_swbd_droideka"
	language.Add(LangName, Name)
	killicon.Add(LangName,"HUD/killicons/default",Color(255,80,0,255))
	language.Add("#"..LangName, Name)
	killicon.Add("#"..LangName,"HUD/killicons/default",Color(255,80,0,255))
end

hook.Add( "PostDrawTranslucentRenderables", "[Impact] Draw Droid Shields", function()

	for k, v in ipairs(ents.FindByClass("npc_vj_swbd_droideka")) do
		if(v:GetNetworkedBool("droid_shield", false)) then
			render.SetMaterial(shieldMat)

			local pos = v:GetPos() + Vector(0, 0, 50)

			render.DrawSphere( pos, 65, 30, 30, Color( 0, 175, 175, 100 ) )
		end
	end

end )