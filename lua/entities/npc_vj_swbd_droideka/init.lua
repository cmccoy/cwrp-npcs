AddCSLuaFile("shared.lua")
include('shared.lua')
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/
ENT.Model = {"models/props/impact/droideka.mdl"} -- The game will pick a random model from the table when the SNPC is spawned | Add as many as you want 
ENT.StartHealth = 500
ENT.HullType = HULL_HUMAN
ENT.VJ_NPC_Class = {"CLASS_STARWARS_CIS"} -- NPCs with the same class with be allied to each other
ENT.HasMeleeAttack = false -- Should the SNPC have a melee attack?
ENT.FootStepTimeRun = 0.25 -- Next foot step sound when it is running
ENT.FootStepTimeWalk = 0.5 -- Next foot step sound when it is walking
ENT.DropWeaponOnDeath = false
ENT.IdleAlwaysWander = true
ENT.CanOpenDoors = true
ENT.DisableWandering = false
ENT.HasShootWhileMoving = true -- Can it shoot while moving?
	-- ====== Sound File Paths ====== --
-- Leave blank if you don't want any sounds to play
ENT.SoundTbl_FootStep = {"vj_battledroid/step1.wav","vj_battledroid/step2.wav","vj_battledroid/step3.wav","vj_battledroid/step4.wav"}
ENT.SoundTbl_Idle = {"vj_battledroid/idle1.wav","vj_battledroid/idle2.wav","vj_battledroid/idle3.wav","vj_battledroid/idle4.wav"}
ENT.SoundTbl_CombatIdle = {"vj_battledroid/talk2.wav","vj_battledroid/talk8.wav","vj_battledroid/talk6.wav"}
ENT.SoundTbl_Alert = {"vj_battledroid/alert1.wav","vj_battledroid/alert2.wav","vj_battledroid/alert3.wav","vj_battledroid/alert4.wav"}
ENT.SoundTbl_OnReceiveOrder = {"vj_battledroid/roger.wav"}
ENT.SoundTbl_WeaponReload = {"vj_battledroid/talk4.wav","vj_battledroid/talk5.wav","vj_battledroid/talk7.wav"}
ENT.SoundTbl_OnGrenadeSight = {"vj_battledroid/grenade1.wav","vj_battledroid/grenade2.wav","vj_battledroid/grenade3.wav","vj_battledroid/grenade4.wav"}
ENT.SoundTbl_OnKilledEnemy = {"vj_battledroid/talk1.wav","vj_battledroid/talk3.wav"}
ENT.SoundTbl_Pain = {"vj_battledroid/pain1.wav","vj_battledroid/pain2.wav","vj_battledroid/pain3.wav","vj_battledroid/pain4.wav","vj_battledroid/pain5.wav","vj_battledroid/pain6.wav","vj_battledroid/pain7.wav","vj_battledroid/pain8.wav","vj_battledroid/pain9.wav",}
ENT.SoundTbl_Impact = {"ambient/energy/spark1.wav","ambient/energy/spark2.wav","ambient/energy/spark3.wav","ambient/energy/spark4.wav"}
ENT.SoundTbl_Death = {"vj_battledroid/death1.wav","vj_battledroid/death2.wav","vj_battledroid/death3.wav","vj_battledroid/death4.wav","vj_battledroid/death5.wav"}

ENT.Weapon_FiringDistanceClose = 10
ENT.Weapon_FiringDistanceFar = 1500
ENT.HasWeaponBackAway = false -- Should the SNPC back away if the enemy is close?
ENT.DisableWeaponFiringGesture = true
ENT.Passive_RunOnDamage = false

	-- ====== Weapon Settings ====== --
ENT.Weapon_NoSpawnMenu = false 
ENT.WeaponSpread = .25
ENT.AllowWeaponReloading = false
ENT.MoveRandomlyWhenShooting = false
-------------------------------------------------------------------------------------------------------------------------------------
function ENT:CustomOnInitialize()
	self:Give("weapon_vj_droideka")
	self.AnimTbl_WeaponAttack = {VJ_SequenceToActivity(self,"idle")}
	self.AnimTbl_IdleStand = {VJ_SequenceToActivity(self,"idle")}
	self.AnimTbl_Walk = {VJ_SequenceToActivity(self,"walk")}
	self.AnimTbl_ShootWhileMovingWalk = {VJ_SequenceToActivity(self,"walk")}
	self.AnimTbl_ShootWhileMovingRun = {VJ_SequenceToActivity(self,"walk")}
	self.AnimTbl_Run = {VJ_SequenceToActivity(self,"run")}

	self:SetNetworkedBool("droid_shield", true)
	self.HasShield = true
	self.ShieldHealth = 2500
end

function ENT:CustomOnTakeDamage_BeforeDamage(dmginfo,hitgroup) 
	local total = dmginfo:GetDamage()
	if(total > self.ShieldHealth && self.HasShield) then
		dmginfo:SetDamage(total - self.ShieldHealth)
		self.HasShield = false
		self.ShieldHealth = 0
		self:SetNetworkedBool("droid_shield", false)
		self:EmitSound("npc/roller/mine/rmine_explode_shock1.wav")
		timer.Simple(60, function()
			self.HasShield = true
			self.ShieldHealth = 2500
			self:SetNetworkedBool("droid_shield", true)
		end)
	elseif(self.HasShield) then
		self.ShieldHealth = self.ShieldHealth - total
		dmginfo:SetDamage(.001)
	end
end

function ENT:CustomOnInitialKilled(dmginfo,hitgroup) 
	ParticleEffect( "striderbuster_break", self:GetPos() + Vector(0,0,50), Angle( 0, 0, 0 ), nil )
	sound.Play("ambient/explosions/explode_7.wav",self:GetPos() + Vector(0,0,50),75,100)

	local timerIdentifier = self:GetCreationID().."_tridroid fade_"..CurTime()
	self:SetRenderMode( RENDERMODE_TRANSCOLOR ) --Allow transparency on model
	self.originalColor = self:GetColor()
	self.curFadeAlpha = self.originalColor.a
	timer.Create(timerIdentifier, .1, 26, function()
		if IsValid(self) then 
			self.curFadeAlpha = self.curFadeAlpha - 10
			self:SetColor( Color( self.originalColor.r, self.originalColor.g, self.originalColor.b, self.curFadeAlpha ) ) 
		else
			timer.Destroy(timerIdentifier)
		end
	end)
end

function ENT:CustomOnTakeDamage_OnBleed(dmginfo,hitgroup)
	if (dmginfo:IsBulletDamage()) then
		local attacker = dmginfo:GetAttacker()
		if math.random(1,2) == 1 then
			if math.random(1,2) == 1 then dmginfo:ScaleDamage(0.50) else dmginfo:ScaleDamage(0.25) end
			self.DamageSpark1 = ents.Create("env_spark")
			self.DamageSpark1:SetKeyValue("Magnitude","1")
			self.DamageSpark1:SetKeyValue("Spark Trail Length","1")
			self.DamageSpark1:SetPos(dmginfo:GetDamagePosition())
			self.DamageSpark1:SetAngles(self:GetAngles())
			//self.DamageSpark1:Fire("LightColor", "255 255 255")
			self.DamageSpark1:SetParent(self)
			self.DamageSpark1:Spawn()
			self.DamageSpark1:Activate()
			self.DamageSpark1:Fire("StartSpark", "", 0)
			self.DamageSpark1:Fire("StopSpark", "", 0.001)
			self:DeleteOnRemove(self.DamageSpark1)
			if self.HasSounds == true && self.HasImpactSounds == true then VJ_EmitSound(self,"vj_impact_metal/bullet_metal/metalsolid"..math.random(1,10)..".wav",70) end
		end
	end
end
/*-----------------------------------------------
	*** Copyright (c) 2012-2017 by DrVrej, All rights reserved. ***
	No parts of this code or any of its contents may be reproduced, copied, modified or adapted,
	without the prior written consent of the author, unless otherwise indicated for stand-alone materials.
-----------------------------------------------*/